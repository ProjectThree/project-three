

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.project.three.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.project.three.UserRole'
grails.plugin.springsecurity.authority.className = 'com.project.three.Role'
//grails.plugin.springsecurity.rememberMe.persistentToken.domainClassName = 'com.project.three.auth.PersistentLogin'
grails.plugin.springsecurity.openid.domainClass = 'com.project.three.OAuthID'
//grails.plugin.springsecurity.dao.reflectionSaltSourceProperty = 'salt'
grails.plugin.springsecurity.userLookup.passwordPropertyName = "passwordHash"
grails.plugin.springsecurity.userLookup.usernamePropertyName = "email"
grails.plugin.springsecurity.userLookup.enabledPropertyName = "enabled"
grails.plugin.springsecurity.userLookup.accountExpiredPropertyName = null
grails.plugin.springsecurity.userLookup.accountLockedPropertyName = null
grails.plugin.springsecurity.userLookup.passwordExpiredPropertyName = null
grails.plugin.springsecurity.successHandler.defaultTargetUrl = '/places'
grails.plugin.springsecurity.failureHandler.defaultFailureUrl = '/user/signin?error'
grails.plugin.springsecurity.auth.loginFormUrl = "/user/signin"

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/place/**',       access: ['permitAll']],
	[pattern: '/templates/**',   access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/user/signin',    access: ['permitAll']],
	[pattern: '/user/signup',    access: ['permitAll']],
	[pattern: '/api/*',      	 access: ['permitAll']],
	[pattern: '/reservation/*',  access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/**',             filters: 'JOINED_FILTERS']
]



// Added by the Spring Security OAuth2 Google Plugin:
grails.plugin.springsecurity.oauth2.domainClass = 'com.project.three.OAuthID'
