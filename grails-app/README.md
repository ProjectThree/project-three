## Setup dev environment
http://docs.grails.org/latest/guide/gettingStarted.html#requirements
* Install sdkman
> curl -s "https://get.sdkman.io" | bash

* get list of installed grails sdk
> sdk l grails

* install our grails version (make it active)
> sdk i grails 3.2.3

* check grails version
> grails --version

## Run app
* from console
> grails run-app

* idea (TODO)



## spring-security-core doc
https://grails-plugins.github.io/grails-spring-security-core/v3/index.html#newInV3
:)