package project.three

import com.project.three.*
import com.project.three.Role
import com.project.three.User

class BootStrap {

    def init = { servletContext ->

        def userRole = Role.findByAuthority(Role.USER) ?: new Role(authority: Role.USER).save(failOnError: true)
        def adminRole = Role.findByAuthority(Role.ADMIN) ?: new Role(authority: Role.ADMIN).save(failOnError: true)

        User user = new User(firstName: "vasya", lastName: "pupkin", email: "vasya@gmail.com", password: "111" )
        user.save(failOnError: true)

        UserRole.create user, userRole


        /*
        Place place = new Place(title: "Salon")
        place.save()

        UserPlace.link(user, place, true)

        def executor = new Executor(title: 'vasil pupkin');
        executor.location = place
        executor.save();
        */

        //def schedule = new Schedule(title: 'vasil pupkin schedule');
        //schedule.executor = executor
        //schedule.save();

        def service = new Service(title: 'haircut');
        service.duration = 60;
        service.price = 20;
        //service.executor = executor
        service.save();

        def service2 = new Service(title: 'shaving');
        service2.duration = 30;
        service2.price = 10;
        //service2.executor = executor
        service2.save();

        def service3 = new Service(title: 'complex');
        service3.duration = 90;
        service3.price = 25;
        //service3.executor = executor
        service3.save();

        /*

        def timeRange = new TimeRange(
                    dayOfWeek: 1, // monday
                    timeStep: 30,
                    schedule: schedule,
                    fromTime: 600,
                    toTime: 1080
            );
        timeRange.save()
        */

        //TimeRangeService.link(timeRange, service)

    }

    def destroy = {
    }
}
