<!doctype html>
<html>
    <head>
        <title><g:message code="page.notfound"/> </title>
        <meta name="layout" content="main">
        <g:if env="development"><asset:stylesheet src="errors.css"/></g:if>
    </head>
    <body>
        <ul class="errors">
            <li><g:message code="page.error"/>: <g:message code="page.notfound"/> (404)</li>
            <li><g:message code="page.path"/>: ${request.forwardURI}</li>
        </ul>
    </body>
</html>
