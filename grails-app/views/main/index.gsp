<!doctype html>
<html>
<head>
    <meta name="layout" content="front"/>
    <title><g:message code="welcome.to.us"/></title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
    <div id="content" role="main" class="container">
        <section class="row colset-2-its" id="home-intro-block">
            

            <asset:image src="css/logo_green.png"/>

            <h1><g:message code="welcome.to.us"/></h1>

            <p id="intro">
                <g:message code="intro"/>
            </p>

        </section>
    </div>

</body>
</html>
