<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="WhenDesk"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">

    <asset:stylesheet src="main.css"/>

    <g:layoutHead/>
</head>
<body>

    <aside id="dashboard-sidebar">
        <a class="dash-navbar-brand" href="/">
          <asset:image src="css/logo.png"/>
        </a>

        <nav id="dashboard-main-menu">
          <g:link controller="place" action="index">
            <span class="dashboard-menu-icon icon-gauge"></span>
            <g:message code="dashboard"/>
          </g:link>

          <g:link controller="place" action="reservations">
            <span class="dashboard-menu-icon icon-book"></span>
            <g:message code="reservation"/>
          </g:link>

          <g:link controller="place" action="schedules">
            <span class="dashboard-menu-icon icon-calendar"></span>
            <g:message code="schedules"/>
          </g:link>

          <g:link controller="place" action="reservations">
            <span class="dashboard-menu-icon icon-contacts"></span>
            <g:message code="contacts"/>
          </g:link>

          <g:link controller="place" action="executors">
            <span class="dashboard-menu-icon icon-group-circled"></span>
            <g:message code="staff"/>
          </g:link>

          <g:link controller="place" action="services">
            <span class="dashboard-menu-icon icon-scissors"></span>
            <g:message code="services"/>
          </g:link>

          <g:link controller="place" action="reservations">
            <span class="dashboard-menu-icon icon-cog-alt"></span>
            <g:message code="settings"/>
          </g:link>

          <g:link controller="user" action="logout">
            <span class="dashboard-menu-icon icon-logout"></span>
            <g:message code="logout"/>
          </g:link>
        </nav>

        <footer id="dashboard-sidebar-footer">
          <div class="copyright">2016-<g:formatDate format="yyyy" date="${new Date()}"/> &copy; <g:message code="footer.allRights"/></div>
        </footer>
    </aside>

    <div id="dashboard-content-wrap">
        <g:layoutBody/>
    </div> <!-- main-wrap -->

    
    <div id="spinner" class="spinner" style="display:none;">
        <g:message code="spinner.alt" default="Loading&hellip;"/>
    </div>

    
    <!-- IE required polyfills, in this exact order -->
    <!--
  <script src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.34.1/es6-shim.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/systemjs/0.19.20/system-polyfills.js"></script>
  <script src="https://npmcdn.com/angular2/es6/dev/src/testing/shims_for_IE.js"></script>
-->
  

  <!-- Angular polyfill required everywhere -->
  <!--<script src="https://code.angularjs.org/2.0.0-beta.17/angular2-polyfills.js"></script>-->
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/angular2-polyfill/0.0.32/angular2-polyfill.js"></script>-->

  <!--<script src="https://code.angularjs.org/tools/system.js"></script>
    <script src="https://code.angularjs.org/tools/typescript.js"></script>-->
    
  <!--<script src="https://code.angularjs.org/2.0.0-beta.17/Rx.js"></script>-->
  <!--
  <script src="https://code.angularjs.org/2.0.0-beta.17/angular2.dev.js"></script>
  <script src="https://code.angularjs.org/2.0.0-beta.17/router.dev.js"></script>
  <script src="https://code.angularjs.org/2.0.0-beta.17/http.dev.js"></script>
-->
  <script>
    document.locale = "${session['org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE'] ? session['org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE']: 'ru'}";
  </script>

  <asset:javascript src="application.js"/>

  <!-- Begin: Custom Page JavaScript Should Go Here -->
  <g:pageProperty name="page.javascript"/>
  <!-- End: Custom Page JavaScript Should Go Here -->


</body>
</html>
