<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="WhenDesk"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">

    <asset:stylesheet src="main.css"/>

    <g:layoutHead/>
</head>
<body>

    <header id="main-header" class="navbar navbar-default navbar-static-top clra" role="navigation">
            <div class="navbar-header navbar-left">
                
                <a class="front-logo-link" href="/#">
                        <asset:image src="css/logo.png"/>
                </a>
            </div>

            <nav class="navbar-collapse collapse nav-bar-left" aria-expanded="false" >
                <div class="lang-bar">
                 <g:select id="lang-select" name="select1" from="${[ ["lang": "en", "name": "English"], [ "lang" : "ru", "name" : "Русский"] ] }" 
                 optionKey="lang" optionValue="name"
                 data-url="${createLink(controller: 'user', action: 'changeLocale')}"
                 value="${session['org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE'] ? session['org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE']: 'ru'}"/>


                  
                  
                </div>
                <ul class="nav navbar-nav navbar-right user-navbar" >
                    <sec:ifNotLoggedIn>
                        <li><g:link controller="user" action="signin"><g:message code="signin"/></g:link></li>
                        <li><g:link controller="user" action="signup"><g:message code="signup"/></g:link></li>
                    </sec:ifNotLoggedIn>
                    <sec:ifLoggedIn>
                        <li><g:link controller="user" action="index"><g:message code="dashboard"/></g:link></li>
                        <li><g:link controller="user" action="logout"><g:message code="logout"/></g:link></li>
                    </sec:ifLoggedIn>
                </ul>
            </nav>
    </header>

   

    <div id="main-wrap" class="container">
        <g:layoutBody/>
    </div> <!-- main-wrap -->

    <footer id="main-footer" class="footer" role="contentinfo">
        <div class="container">
            <div class="copyright">2016-<g:formatDate format="yyyy" date="${new Date()}"/> &copy; <g:message code="footer.allRights"/></div>
        </div>  
    </footer>

    <div id="spinner" class="spinner" style="display:none;">
        <g:message code="spinner.alt" default="Loading&hellip;"/>
    </div>

    
    <!-- IE required polyfills, in this exact order -->
    <!--
  <script src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.34.1/es6-shim.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/systemjs/0.19.20/system-polyfills.js"></script>
  <script src="https://npmcdn.com/angular2/es6/dev/src/testing/shims_for_IE.js"></script>
-->
  

  <!-- Angular polyfill required everywhere -->
  <!--<script src="https://code.angularjs.org/2.0.0-beta.17/angular2-polyfills.js"></script>-->
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/angular2-polyfill/0.0.32/angular2-polyfill.js"></script>-->

  <!--<script src="https://code.angularjs.org/tools/system.js"></script>
    <script src="https://code.angularjs.org/tools/typescript.js"></script>-->
    
  <!--<script src="https://code.angularjs.org/2.0.0-beta.17/Rx.js"></script>-->
  <!--
  <script src="https://code.angularjs.org/2.0.0-beta.17/angular2.dev.js"></script>
  <script src="https://code.angularjs.org/2.0.0-beta.17/router.dev.js"></script>
  <script src="https://code.angularjs.org/2.0.0-beta.17/http.dev.js"></script>
-->
  <script>
    document.locale = "${session['org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE'] ? session['org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE']: 'ru'}";
  </script>

  <asset:javascript src="application.js"/>

  <!-- Begin: Custom Page JavaScript Should Go Here -->
  <g:pageProperty name="page.javascript"/>
  <!-- End: Custom Page JavaScript Should Go Here -->


</body>
</html>
