<!doctype html>
<html>
<head>
    <meta name="layout" content="front"/>
    <title>Reservation</title>
</head>
<body>
  
  <g:if  test="${schedule}">
    <schedule data-id="${schedule.id}"><g:message code="loading"/>...</schedule>
  </g:if>

   <!-- Begin: Custom Page JavaScript Should Go Here -->
  <content tag="javascript">
   <script src="${createLink(controller: 'api', action: 'routes')}"></script>
     <script src="https://unpkg.com/core-js/client/shim.min.js"></script>

    <script src="https://unpkg.com/zone.js@0.7.4?main=browser"></script>
    <script src="https://unpkg.com/systemjs@0.19.39/dist/system.src.js"></script>
    <!--<script src="systemjs.config.js"></script>-->
    <asset:javascript src="angular/systemjs.config.js"/>
    <script>
      System.import('/assets/angular/app/main.schedule.ts').catch(function(err){ console.error(err); });
    </script>

    
  </content>
  
</body>
</html>
