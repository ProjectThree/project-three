<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>New reservation</title>
</head>
<body>

	<h1>New reservation</h1>
	 <g:form  url="[controller:'reservation', action:'add']" >
        
        <g:hiddenField name="scheduleId" value="${schedule?.id}" />
        <g:hiddenField name="time" value="${time}" />

        <div class="form-row">
          Service:  
          <g:select name="serviceId"
          from="${timeRange.getServices()}"
          optionValue="title"
          optionKey="id" />

        </div>    

        <g:actionSubmit value="Reserve" action="add" />
	</g:form>
    

</body>
</html>
