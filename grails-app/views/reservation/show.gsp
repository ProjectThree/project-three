<!doctype html>
<html>
<head>
    <meta name="layout" content="front"/>
    <title>Reservation</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>

    <div>
        ID: ${reservation.id}

    </div>

    <div>
    	Reserved:  <g:formatDate format="yyyy-MM-dd HH:mm" date="${reservation.dateReserved}"/>
    </div>

    <div>
        Duration: ${reservation.sessionDuration}

    </div>

    <div>
        Service: ${reservation.service?.title}

    </div>




</body>
</html>
