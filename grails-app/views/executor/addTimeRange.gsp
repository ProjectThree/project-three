<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>New time range</title>
</head>
<body>

	<h1>New time range</h1>
	 <g:form  url="[controller:'executor', action:'addTimeRange']" >
        
        <g:hiddenField name="scheduleId" value="${schedule?.id}" />
		
		<div class="form-row">
			<label>Day: </label>
               <%@ page import="com.project.three.WeekDay" %>
               <g:select name="dayOfWeek" id="dayOfWeek"
                    from="${WeekDay}" 
                    
                    optionValue="key"
                    optionKey="key"
                    />
  
        </div>

        <div class="form-row">
            <label>From time: </label>
            <g:field name="from_time"  value="" type="time" />
        </div>

        <div class="form-row">
            <label>To time: </label>
            <g:field name="to_time"  value="" type="time" />
        </div>

        <div class="form-row">
            <label>Time step: </label>
            <g:field name="timeStep"  value="${timeRange?.timeStep}" type="number" /> minutes
        </div>

        <div class="form-row">
            <label>Services: </label>

            <g:each in="${executor.getServices()}" var="service" status="i">
                <g:checkBox name="services" value="${service.id}" checked="${false}" />
                <label for="services">${service.title}</label>
            </g:each>

        </div>

        
       
        
        <g:actionSubmit value="Add" action="addTimeRange" />
	</g:form>
    

</body>
</html>
