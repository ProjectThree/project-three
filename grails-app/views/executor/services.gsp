<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Services</title>
</head>
<body>
  <h1>Services for "${executor.title}":</h1>

  <g:link controller="executor" action="addService" params="[executorId: executor.id]">Add new Service</g:link>
  <table class="dashboard-table">
      <tr class="table-headrow">
        <td>Services</td>
        <td>Duration</td>
        <td>Price</td>
        
      </tr>

  	<g:each var="service" in="${services}">
  		<tr>
  			<td>${service.title}</td>
        <td>${service.duration}</td>
        <td>${service.price}</td>
  		</tr>
  	</g:each>
  </table>
    

</body>
</html>
