<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>New service</title>
</head>
<body>

	<h1>New service</h1>
	 <g:form  url="[controller:'executor', action:'addService']" >
        
        <g:hiddenField name="executorId" value="${executor?.id}" />
		
		<div class="form-row">
			<label>Title: </label>
        	<g:textField name="title" value="${service?.title}"/><br/>
        </div>

        <div class="form-row">
			<label>Duration: </label>
        	<g:field name="duration"  value="${service?.duration}" type="number" />
        </div>

        <div class="form-row">
			<label>Price: </label>
        	<g:textField name="price" value="${service?.price}"/><br/>
        </div>
       
        
        <g:actionSubmit value="Add" action="addService" />
	</g:form>
    

</body>
</html>
