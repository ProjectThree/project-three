<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>New executor</title>
</head>
<body>

	<h1>New executor</h1>
	 <g:form  url="[controller:'executor', action:'add']" >
        
        <g:hiddenField name="placeId" value="${place?.id}" />

        <g:hasErrors bean="${executor}">
            <div class="errors">
                <g:renderErrors bean="${executor}"/>
            </div>
        </g:hasErrors>
		
		<div class="form-row">
			<label>Title: </label>
        	<g:textField name="title" value="${executor?.title}"/><br/>
        </div>
       
        
        <g:actionSubmit value="Add" action="add" />
	</g:form>
    

</body>
</html>
