<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>New schedule</title>
</head>
<body>

	<h1>New schedule</h1>
	 <g:form  url="[controller:'executor', action:'addSchedule']" >
        
        <g:hiddenField name="executorId" value="${executor?.id}" />
		
		<div class="form-row">
			<label>Title: </label>
        	<g:textField name="title" value="${schedule?.title}"/><br/>
        </div>
       
        
        <g:actionSubmit value="Add" action="addSchedule" />
	</g:form>
    

</body>
</html>
