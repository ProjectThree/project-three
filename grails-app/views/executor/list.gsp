<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Executors</title>
</head>
<body>
  <h1>Executors for "${place.title}":</h1>

  <g:link controller="executor" action="add" params="[placeId: place.id]">Add new executor</g:link>
  <table class="dashboard-table">
      <tr class="table-headrow">
        <td>Executor</td>
        <td></td>
        <td></td>
        <td></td>
      </tr>

  	<g:each var="executor" in="${executors}">
  		<tr>
  			<td>${executor.title}</td>
        <td><g:link controller="executor" action="edit" params="[id: executor.id]">Edit</g:link></td>
        <td><g:link controller="executor" action="schedule" params="[executorId: executor.id]">Schedule</g:link></td>
        <td><g:link controller="executor" action="addTimeRange" params="[executorId: executor.id]">Add time range</g:link></td>
  		</tr>
  	</g:each>
  </table>
    

</body>
</html>
