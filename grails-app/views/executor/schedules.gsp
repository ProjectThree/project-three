<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Schedules</title>
</head>
<body>
  <h1>Schedules for "${executor.title}":</h1>

  <g:link controller="executor" action="addSchedule" params="[executorId: executor.id]">Add new schedule</g:link>
  <table class="dashboard-table">
      <tr class="table-headrow">
        <td>Schedules</td>
        <td></td>
        <td></td>
        <td></td>
        
      </tr>

  	<g:each var="schedule" in="${schedules}">
  		<tr>
  			<td>${schedule.title}</td>
        <td><g:link controller="executor" action="schedule" params="[scheduleId: schedule.id]">Schedule</g:link></td>
        <td><g:link controller="executor" action="addTimeRange" params="[scheduleId: schedule.id]">Add time range</g:link></td>
        <td><g:link controller="reservation" action="schedule" params="[scheduleId: schedule.id]">Check live</g:link></td>
  		</tr>
  	</g:each>
  </table>
    

</body>
</html>
