<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Edit executor</title>
</head>
<body>

	<h1>Edit executor</h1>

    <td><g:link controller="executor" action="list" params="[placeId: executor.location.id]">Back to executors</g:link></td>

	 <g:form  url="[controller:'executor', action:'edit']" >
        
        <g:hiddenField name="id" value="${executor?.id}" />

        <g:if test="${flash.message}">
          <div class="message">${flash.message}</div>
        </g:if>
		
		<div class="form-row">
			<label>Title: </label>
        	<g:textField name="title" value="${executor?.title}"/><br/>
        </div>
        
        <g:actionSubmit value="Save" action="edit" />
	</g:form>
    

</body>
</html>
