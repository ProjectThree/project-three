<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Schedule</title>
</head>
<body>
  <h1>"${schedule.title}" schedule:</h1>
  <div>
    <td><g:link controller="executor" action="addTimeRange" params="[scheduleId: schedule.id]">Add time range</g:link></td>
  </div>
  
  <div class="dashboard-schedule fr-box clra">
    <g:each var="scheduleItem" in="${scheduleItems}">
      <div class="schedule-day-column">
        <div class="schedule-day-label">${scheduleItem.day}</div>
        <g:each var="time" in="${scheduleItem.times}">
          <div class="time">${time.label}</div>
        </g:each>

      </div>
    </g:each>
  </div>  
  
</body>
</html>
