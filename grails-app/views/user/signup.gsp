<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="signup.extended"/></title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>

	<h1><g:message code="signup.extended"/></h1>
	 <g:form  url="[controller:'user', action:'signup']" >

        <g:hasErrors bean="${user}">
            <div class="errors">
				%{--TODO error codes must be i18n too--}%
                <g:renderErrors bean="${user}"/>
            </div>
        </g:hasErrors>
		
		<div class="form-row">
			<label><g:message code="signup.firstName"/>: </label>
        	<g:textField name="firstName" value="${user?.firstName}"/><br/>
        </div>
        
        <div class="form-row">
        	<label><g:message code="signup.lastName"/>: </label>
        	<g:textField name="lastName"  value="${user?.lastName}" /><br/>
        </div>	

        <div class="form-row">
        	<label>Email: </label>
        	<g:textField name="email"  value="${user?.email}"/>
    	</div>

    	<div class="form-row">
    		<label><g:message code="password"/>: </label>
    		<g:passwordField  name="password"  value="${user?.password}" />
    	</div>

    	<div class="form-row">
    		<label><g:message code="passwordConfirm"/>: </label>
    		<g:passwordField value="${params?.confirm}" name="confirm" />
    	</div>
        
	<div class="form-row">
        	<g:actionSubmit value="${message(code:'signup')}" action="signup"/>
	</div>

	</g:form>
    

</body>
</html>
