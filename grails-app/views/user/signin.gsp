<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="login"/></title>
</head>
<body>

   <h1><g:message code="login"/></h1>
	<g:if test="${flash.message}">
		%{--TODO flash.message could return error code--}%
		<div class="message">${flash.message}</div>
	</g:if>
<sec:ifLoggedIn>
	User name: <sec:username/>
</sec:ifLoggedIn>

<sec:ifNotLoggedIn>
<g:form method="POST" uri="/login/authenticate">
	<div class="form-row">
	<label>Email: </label>
	<g:textField name="username"  value="${user?.email}"/>
	</div>
	<div class="form-row">
	<label><g:message code="password"/>: </label>
		<input name="password" type="password"/>
	</div>
	<div class="form-row">
	<g:submitButton name="login" value="${message(code:'login')}"/>
	</div>

	<div class="form-row">
		<g:message code="or"/>
	</div>

	<div class="form-row">
		<oauth2:connect provider="google" id="google-connect-link"><g:message code="logInWith"/> Google</oauth2:connect>
	</div>
	<div class="form-row">

		<oauth2:connect provider="facebook" id="facebook-connect-link"><g:message code="logInWith"/> Facebook</oauth2:connect>

		<%--
		<oauth2:ifLoggedInWith provider="google">yes</oauth2:ifLoggedInWith>
		<oauth2:ifNotLoggedInWith provider="google">no</oauth2:ifNotLoggedInWith>
		--%>

	</div>

	

</g:form>
</sec:ifNotLoggedIn>

</body>
</html>
