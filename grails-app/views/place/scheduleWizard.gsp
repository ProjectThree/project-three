<!doctype html>
<html>
<head>
    <meta name="layout" content="dash"/>
    <title><g:message code="title.scheduleWizard"/></title>
</head>
<body>

  <h1><g:message code="title.scheduleWizard"/></h1>
  <schedule-wizard data-id="${scheduleId}"><g:message code="loading"/>...</schedule-wizard>
  

  <!-- Begin: Custom Page JavaScript Should Go Here -->
  <content tag="javascript">
	 <script src="${createLink(controller: 'api', action: 'routes')}"></script>
  	 <script src="https://unpkg.com/core-js/client/shim.min.js"></script>

    <script src="https://unpkg.com/zone.js@0.7.4?main=browser"></script>
    <script src="https://unpkg.com/systemjs@0.19.39/dist/system.src.js"></script>
    <!--<script src="systemjs.config.js"></script>-->
    <asset:javascript src="angular/systemjs.config.js"/>
    <script>
      //System.import('/assets/angular/app/main.wizard.js').catch(function(err){ console.error(err); });
      System.import('/assets/angular/app/main.wizard.ts').catch(function(err){ console.error(err); });
    </script>

  	
  </content>
  <!-- End: Custom Page JavaScript Should Go Here -->
  
</body>
</html>
