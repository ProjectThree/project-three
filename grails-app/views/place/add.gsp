<!doctype html>
<html>
<head>
    <meta name="layout" content="dash"/>
    <title><g:message code="user.addNewPlace"/></title>
</head>
<body>

	<h1><g:message code="user.addNewPlace"/></h1>
	 <g:form  url="[controller:'place', action:'add']" >

        <g:hasErrors bean="${place}">
            <div class="errors">
                <g:renderErrors bean="${place}"/>
            </div>
        </g:hasErrors>
		
		<div class="form-row">
			<label><g:message code="title"/>: </label>
        	<g:textField name="title" value="${place?.title}"/><br/>
        </div>
        
        <div class="form-row">
        	<label><g:message code="address"/>: </label>
        	<g:textField name="address"  value="${place?.address}" /><br/>
        </div>	

        <div class="form-row">
        	<label><g:message code="phoneNumber"/>: </label>
        	<g:textField name="phoneNumber"  value="${place?.phoneNumber}"/>
    	</div>


        
        <g:actionSubmit value="Add" action="add" />
	</g:form>
    

</body>
</html>
