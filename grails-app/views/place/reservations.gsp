<!doctype html>
<html>
<head>
    <meta name="layout" content="dash"/>
    <title><g:message code="reservation"/></title>
</head>
<body>
  <h1><g:message code="reservation"/>:</h1>
  
  %{--
  <table class="dashboard-table">
      <tr class="table-headrow">
        <td><g:message code="place"/></td>
        <td><g:message code="executor"/></td>
        <td><g:message code="service"/></td>
        <td><g:message code="time"/></td>
        <td><g:message code="status"/></td>
        
      </tr>

  	<g:each var="reservation" in="${reservations}">
  		<tr>
  			<td>${reservation.location.title}</td>
        <td>${reservation.executor ? reservation.executor.title : ''}</td>
        <td>${reservation.service && reservation.service.title  ? reservation.service.title : reservation.serviceName}</td>
        <td>
          <g:formatDate format="yyyy-MM-dd HH:mm" date="${reservation.dateReserved}"/>
          <div>(${reservation.sessionDuration} min)</div>
        </td>
        <td>${ reservation.getStatusLabel() }</td>
  		</tr> 
  	</g:each>
  </table>
  --}%


  <reservation-control><g:message code="loading"/>...</reservation-control>
  

  
  <content tag="javascript">
    <script src="${createLink(controller: 'api', action: 'routes')}"></script>
    <script src="https://unpkg.com/core-js/client/shim.min.js"></script>
    <script src="https://unpkg.com/zone.js@0.7.4?main=browser"></script>
    <script src="https://unpkg.com/systemjs@0.19.39/dist/system.src.js"></script>
    <asset:javascript src="angular/systemjs.config.js"/>
    <script>
      System.import('/assets/angular/app/main.reservation.ts').catch(function(err){ console.error(err); });
    </script>
  </content>
    

</body>
</html>
