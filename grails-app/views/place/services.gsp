<!doctype html>
<html>
<head>
    <meta name="layout" content="dash"/>
    <title><g:message code="services"/></title>
</head>
<body>
  
  <services-control><g:message code="loading"/>...</services-control>
  

  
  <content tag="javascript">
    <script src="${createLink(controller: 'api', action: 'routes')}"></script>
    <script src="https://unpkg.com/core-js/client/shim.min.js"></script>
    <script src="https://unpkg.com/zone.js@0.7.4?main=browser"></script>
    <script src="https://unpkg.com/systemjs@0.19.39/dist/system.src.js"></script>
    <asset:javascript src="angular/systemjs.config.js"/>
    <script>
      System.import('/assets/angular/app/main.services.ts').catch(function(err){ console.error(err); });
    </script>
  </content>
    

</body>
</html>
