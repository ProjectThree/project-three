<!doctype html>
<html>
<head>
    <meta name="layout" content="dash"/>
    <title><g:message code="places"/></title>
</head>
<body>
  <h1><g:message code="places"/></h1>
  <g:link controller="place" action="add">
    <g:message code="user.addNewPlace"/>
  </g:link>
  
  <table class="dashboard-table">
      <tr class="table-headrow">
        <td><g:message code="place"/></td>
        <td>Executors</td>
        <td><g:message code="action"/></td>
      </tr>

  	<g:each var="place" in="${places}">
  		<tr>
  			<td>${place.title}</td>
        <td><g:link controller="executor" action="executors" params="[placeId: place.id]">Executors</g:link></td>
        
        <td>
          <g:link controller="place" action="edit" params="[id: place.id]"><g:message code="edit"/></g:link>
          &nbsp;|&nbsp;
          <g:link controller="place" action="scheduleWizard" params="[placeId: place.id]"><g:message code="user.addNewSchedule"/></g:link>
          &nbsp;|&nbsp;
          <g:link controller="place" action="schedules" params="[placeId: place.id]"><g:message code="schedules"/></g:link>
          
          
        </td>
  		</tr>
  	</g:each>
  </table>
    

</body>
</html>
