<!doctype html>
<html>
<head>
    <meta name="layout" content="dash"/>
    <title><g:message code="schedules"/></title>
</head>
<body>
  %{--  
  <h1>
    <g:if test="${place}">
      ${place.title} 
    </g:if>

    <g:message code="schedules"/>:
  </h1>

  <g:link controller="place" action="scheduleWizard" params="[placeId: place ? place.id : null]"><g:message code="user.addNewSchedule"/></g:link>

  <table class="dashboard-table">
      <tr class="table-headrow">
        <td><g:message code="schedule"/></td>
        <td><g:message code="code"/></td>
        <td><g:message code="private"/></td>
        <td><g:message code="action"/></td>
        
      </tr>

  	<g:each var="schedule" in="${schedules}">
  		<tr>
  			<td>${schedule.title}</td>
        <td>${schedule.code}</td>
        <td>${schedule.confidential}</td>
        <td>
          <g:link controller="place" action="editSchedule" params="[id: schedule.id]"><g:message code="edit"/></g:link>
          &nbsp;|&nbsp;
          <g:link controller="place" action="removeSchedule" params="[id: schedule.id]"><g:message code="remove"/></g:link>
          &nbsp;|&nbsp;
          <g:link controller="reservation" action="schedule" params="[id: schedule.id]"><g:message code="link"/></g:link>
        </td>
  		</tr>
  	</g:each>
  </table>
  --}%

  <schedules-control><g:message code="loading"/>...</schedules-control>

  <!-- Begin: Custom Page JavaScript Should Go Here -->
  <content tag="javascript">
    <script src="${createLink(controller: 'api', action: 'routes')}"></script>
    <script src="https://unpkg.com/core-js/client/shim.min.js"></script>
    <script src="https://unpkg.com/zone.js@0.7.4?main=browser"></script>
    <script src="https://unpkg.com/systemjs@0.19.39/dist/system.src.js"></script>
    
    <asset:javascript src="angular/systemjs.config.js"/>
    <script>
      System.import('/assets/angular/app/main.schedules.ts').catch(function(err){ console.error(err); });
    </script>

    
  </content>
  <!-- End: Custom Page JavaScript Should Go Here -->
    

</body>
</html>
