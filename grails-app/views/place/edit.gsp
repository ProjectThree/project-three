<!doctype html>
<html>
<head>
    <meta name="layout" content="dash"/>
    <title>Edit place</title>
</head>
<body>

	<h1>Edit place</h1>
	 <g:form  url="[controller:'place', action:'edit']" >
        
        <g:hiddenField name="id" value="${place?.id}" />

        <g:if test="${flash.message}">
          <div class="message">${flash.message}</div>
        </g:if>
		
		<div class="form-row">
			<label>Title: </label>
        	<g:textField name="title" value="${place?.title}"/><br/>
        </div>
        
        <div class="form-row">
        	<label>Address: </label>
        	<g:textField name="address"  value="${place?.address}" /><br/>
        </div>	

        <div class="form-row">
        	<label>Phone number: </label>
        	<g:textField name="phoneNumber"  value="${place?.phoneNumber}"/>
    	</div>


        
        <g:actionSubmit value="Save" action="edit" />
	</g:form>
    

</body>
</html>
