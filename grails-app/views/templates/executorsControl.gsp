<h1>
  <g:if test="${place}">
  ${place.title} 
</g:if>

<g:message code="executors"/>:
</h1>

<a href="#" (click)="addExecutorClick(event)"><g:message code="addExecutor"/></a>

<table class="dashboard-table">
  <tr class="table-headrow">
    <td><g:message code="executor"/></td>
    <td><g:message code="action"/></td>

  </tr>


  <tr *ngFor="let executor of executors">

    <td>{{ executor.name }}</td>
    
    <td>
      <a href="${createLink(controller: 'place', action: 'removeExecutor')}/{{ executor.id }}"><g:message code="remove"/></a>
    </td>
  </tr>

</table>

<div *ngIf="showEmbedModal" class="control-modal-overlay" (click)="modalOverlayClick($event)">
 <div class="control-modal">
  <g:message code="name"/>
  <input [(ngModel)]="newExecutorName" />
  <button (click)="addNewExecutor()"><g:message code="add"/></button>
 </div>
</div>
