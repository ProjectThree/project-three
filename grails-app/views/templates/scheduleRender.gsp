<div class="schedule-render-week-controls clra" *ngIf="mode != 'edit'">
  <a href="#" class="schedule-render-week-control-prev"  *ngIf="daysOffset > 0" (click)="jumpWeek(-1)">← <g:message code="prevWeek"/></a>
  <a href="#" class="schedule-render-week-control-next" *ngIf="daysOffset < daysOffsetLimit" (click)="jumpWeek(1)"><g:message code="nextWeek"/> →</a>
</div>

<a *ngIf="switchModeAvailable" href="#" (click)="switchEditMode()" >Switch mode: {{ mode }}</a>

<div class="fr-box schedule-render-day-columns" [ngClass]="'fr-box-child-count-'+daysToShow">
	<div *ngFor="let workingDay of workingDays" class="schedule-render-day-column">
		<div class="schedule-render-day-title">
			<span>{{workingDay.name | translate }}</span>  
			<span *ngIf="mode != 'edit'">{{ workingDay.date | date : "MM-dd" }}</span>
		</div>
		<div class="schedule-render-day-times"> 
			<div *ngFor="let dayTime of workingDay.times" 
				class="schedule-render-day-time"  
				[ngClass]="[
				dayTime.beforeBreak ? 'time-before-break' : ''  ,
				dayTime.isReserved ? 'disabled' : '', 
				dayTime.selected ? 'selected': '',
				dayTime.interimSelected ? 'interim-selected' : '',
				dayTime.timeBlock ? 'is-time-block': ''
				 ]" 
				
				> 

				<span class="schedule-render-day-time-label" (click)="dayTimeClick(workingDay, dayTime, $event)">{{ dayTime.label }}</span>

				<div [ngClass]="[dayTime.timeBlock ? 'block-levels-' + dayTime.timeBlockIndex : '']" 
				class="schedule-render-day-time-block flexbox-column" 
				*ngIf="dayTime.timeBlock" (click)="dayTime.timeBlock.options.length == 1 && dayTimeClickWithBlock(workingDay, dayTime, dayTime.timeBlock, 0, $event)" >
					<div>{{ dayTime.timeBlock.timeRange.getPeriodLabel() }}</div>
					<%--
					<div>{{ dayTime.timeBlock.title }}</div>
					<div *ngIf="dayTime.timeBlock.showLeftBookingCount"> 
						<g:message code="bookingLeft"/>: {{ dayTime.timeBlock.leftBookingCount }}
					</div>
					--%>
					<div class="schedule-render-time-block-options flexbox-row">
						<div *ngFor="let timeBlockOption of dayTime.timeBlock.options; let i = index" class="schedule-render-time-block-option" 
						[ngClass]="[
							timeBlockOption.isReserved ? 'disabled' : ''
				 		]" 

 						(click)="dayTimeClickWithBlock(workingDay, dayTime, dayTime.timeBlock, i, $event)"
						>
							<div>{{ timeBlockOption.title }}</div>
							<div *ngIf="timeBlockOption.showLeftBookingCount"> 
								<g:message code="bookingLeft"/>: {{ timeBlockOption.leftBookingCount }}
							</div>
						</div>
					</div>

				</div>
			
				<!-- !!!! wtf?! refactor this -->
				<div *ngIf="dayTime.active" class="schedule-render-manage-time-block-links">
					<a href="#" (click)="manageTimeBlockClick($event)">
						<g:message code="wizard.manageTimeBlock"/>
					</a>
				</div>

			</div>
		</div>
		 
	</div>                

</div>

<%-- MODALS --%>
<div *ngIf="showManageTimeBlockModal" class="control-modal-overlay" (click)="manageTimeBlockOverlayClick($event)">
	<div class="control-modal" *ngIf="selectedDayTimes">
		
		<div *ngFor="let timeBlockOption of timeBlockOptions" class="schedule-wizard-time-block-option">
			<div class="schedule-wizard-popup-row">
				<span class="schedule-wizard-label">
					<g:message code="title"/>:
				</span>
				<input [(ngModel)]="timeBlockOption.title" />

				<a *ngIf="timeBlockOptions.length > 1" href="javascript:;" (click)="removeTimeBlockOption(timeBlockOption)">
					<g:message code="wizard.remove"/>
				</a>
			</div>
		
			<div *ngIf="multiBooking" class="schedule-wizard-popup-row">
				<span class="schedule-wizard-label">
					<g:message code="wizard.multiBookingMaxNumber"/>: 
				</span>
				<input [(ngModel)]="timeBlockOption.multiBookingMaxNumber" min="1" type="number" />
			</div>
		</div>

		<div>
			<a href="#" (click)="addTimeBlockOption()">
				+ <g:message code="addOption"/> 
			</a>
		</div>

		<div>

			<button *ngIf="!currentBookingTimeBlock" class="schedule-wizard-btn" (click)="createTimeBlock()"  [class.disabled]="false">
				<g:message code="wizard.createTimeBlock"/>
			</button>

			<div *ngIf="currentBookingTimeBlock">
				<button class="schedule-wizard-btn" (click)="updateTimeBlock()"  [class.disabled]="false">
					<g:message code="wizard.updateTimeBlock"/>
				</button>

				<button class="schedule-wizard-btn" (click)="removeTimeBlock()" >
					<g:message code="remove"/>
				</button>
			</div>	
		</div>

	</div>
</div>	


<div *ngIf="showReservationModal" class="schedule-render-reservation-modal-overlay" (click)="reservationOverlayClick($event)">
 <div class="schedule-render-reservation-modal" *ngIf="currentReservation">
    <form method="post">
      <input type="hidden" value="{{ currentReservation.time.time }}" name="time" />

      <%-- SINGLE SERVICE --%>
      <%--
      <div class="schedule-render-reservation-modal-row" *ngIf="services && services.length == 1">
      	{{service.name}}
      </div>
      --%>
 	  
 	  <%-- MULTIPLE SERVICES --%>
 	  <div class="schedule-render-reservation-modal-row" *ngIf="services && services.length > 1">
 	  	<select name="service" [(ngModel)]="currentReservation.service" (change)="serviceChange()" >
 	  		<option [ngValue]="service" seleted>-- <g:message code="selectService"/> --</option>
        	<option *ngFor="let service of services" [ngValue]="service">  
        		{{service.name}}
        	</option>
    	</select>
 	  </div>

 	  <div *ngIf="currentReservation && currentReservation.service">
 	    <div class="schedule-render-reservation-modal-row" >
 	  	  <span>
 	  		<strong>{{ currentReservation.service.name }}</strong> (Price: <strong>{{ getReservationFormattedPrice() }}</strong>)
 	  	  </span>
 	  	
 	  	  at 
 	  	  <strong>{{ currentReservation.day.name | translate}}, {{ currentReservation.day.date | date : "yyyy-MM-dd" }} , 
 	  	  {{ currentBookingTimeBlock ? currentBookingTimeBlock.timeRange.getPeriodLabel() : currentReservation.time.label }}
 	  	  </strong>
 	    </div>

 	    <div class="schedule-render-reservation-modal-row" *ngIf="currentReservation.showLeftBookingCount">
 	    	<g:message code="bookingLeft"/>: {{ currentReservation.leftBookingCount }}
 	    </div>

 	    <div *ngIf="currentBookingTimeBlock && currentBookingTimeBlockOption">
 	    	<strong>{{ currentBookingTimeBlockOption.title }}</strong>
 	    </div>

		<div class="schedule-render-reservation-modal-row clra">
			<div class="schedule-render-reservation-modal-label">
				<g:message code="name"/>
			</div>
			<input name="clientName" type="text" class="schedule-render-reservation-modal-field" [(ngModel)]="currentReservation.clientInfo.clientName" />
		</div>

		<div class="schedule-render-reservation-modal-row clra">
			<div class="schedule-render-reservation-modal-label">
				<g:message code="phone"/>
			</div>
			<input name="clientPhone" type="tel" class="schedule-render-reservation-modal-field" [(ngModel)]="currentReservation.clientInfo.clientPhone" />
			
		</div>

		<div class="schedule-render-reservation-modal-row clra">
			<div class="schedule-render-reservation-modal-label">
				<g:message code="email"/>
			</div>
			<input  name="clientEmail" type="email" class="schedule-render-reservation-modal-field" [(ngModel)]="currentReservation.clientInfo.clientEmail" />
			
		</div> 	    

 	  	<div class="schedule-render-reservation-modal-row">
 	  		<button (click)="bookClick($event)"><g:message code="book"/></button>
 	  	</div>
 	  </div>
 	  

 	  
 	</form>
 </div>
</div>