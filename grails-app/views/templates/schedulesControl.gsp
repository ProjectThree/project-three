<h1>
  <g:if test="${place}">
  ${place.title} 
</g:if>

<g:message code="schedules"/>:
</h1>

<g:link controller="place" action="scheduleWizard"><g:message code="user.addNewSchedule"/></g:link>

<table class="dashboard-table">
  <tr class="table-headrow">
    <td><g:message code="schedule"/></td>
    <td><g:message code="code"/></td>
    <td><g:message code="private"/></td>
    <td><g:message code="action"/></td>

  </tr>


  <tr *ngFor="let schedule of schedules">

    <td>{{ schedule.title }}</td>
    <td>{{ schedule.code }}</td>
    <td>{{ schedule.confidential }}</td>
    <td>
      <a href="${createLink(controller: 'place', action: 'scheduleWizard')}/{{ schedule.id }}"><g:message code="edit"/></a>

      &nbsp;|&nbsp;
      <a href="${createLink(controller: 'place', action: 'removeSchedule')}/{{ schedule.id }}"><g:message code="remove"/></a>
      &nbsp;|&nbsp;
      <a href="${createLink(controller: 'reservation', action: 'schedule')}/{{ schedule.id }}"><g:message code="link"/></a>
      &nbsp;|&nbsp;
      <a href="javascript:;" (click)="embedClick(schedule, $event)"><g:message code="embed"/></a>

    </td>
  </tr>

</table>

<div *ngIf="showEmbedModal" class="control-modal-overlay" (click)="modalOverlayClick($event)">
 <div class="control-modal" *ngIf="scheduleForEmbeding">
  <g:message code="link"/>
  <input value="${createLink(absolute: true, uri: "/desk")}/{{ scheduleForEmbeding.code }}" />
 </div>
</div>
