<h1>
  <g:if test="${place}">
  ${place.title} 
</g:if>

<g:message code="services"/>:
</h1>

<a href="#" (click)="addServiceClick(event)"><g:message code="addService"/></a>

<table class="dashboard-table">
  <tr class="table-headrow">
    <td><g:message code="service"/></td>
    <td><g:message code="duration"/></td>
    <td><g:message code="price"/></td>
    <td><g:message code="action"/></td>

  </tr>


  <tr *ngFor="let service of services">

    <td>{{ service.name }}</td>
    <td>{{ service.duration }}</td>
    <td>{{ service.price }}</td>
    
    <td>
      <a href="${createLink(controller: 'place', action: 'removeService')}/{{ service.id }}"><g:message code="remove"/></a>
    </td>
  </tr>

</table>

<div *ngIf="showEmbedModal" class="control-modal-overlay" (click)="modalOverlayClick($event)">
 <div class="control-modal">
    <div class="schedule-render-modal-row">
      <span class="schedule-render-modal-label">
        <g:message code="name"/>
      </span>
      <input [(ngModel)]="newServiceName" class="schedule-render-modal-field" />
    </div>

    <div class="schedule-render-modal-row">
      <span class="schedule-render-modal-label">
        <g:message code="duration"/>
      </span>
      <input [(ngModel)]="newServiceDuration" type="number" step="1" min="0" class="schedule-render-modal-field" />
    </div>

    <div class="schedule-render-modal-row">
      <span class="schedule-render-modal-label">
        <g:message code="price"/>
      </span>
      <input [(ngModel)]="newServicePrice" type="number" step="0.01" min="0" class="schedule-render-modal-field" />
    </div>

    <button (click)="addNewService()"><g:message code="add"/></button>
 </div>
</div>
