<table class="dashboard-table">
      <tr class="table-headrow">
        <td><g:message code="service"/></td>
        <td><g:message code="time"/></td>
        <td><g:message code="client"/></td>
        <td><g:message code="price"/></td>
        <td><g:message code="status"/></td>
        
      </tr>

    <tr *ngFor="let reservation of reservations">
        <td>{{ reservation.service && reservation.service.title  ? reservation.service.title : reservation.serviceName }}</td>
        <td>
          <span>{{ reservation.dateReserved }}</span>
          <div>({{ reservation.sessionDuration + " " + ( "min" | translate ) }} )</div>
          <div *ngIf="reservation.multiBooking">[multi]</div>
        </td>
        <td>
          <strong *ngIf="reservation.clientName">{{ reservation.clientName }}</strong>
          <div *ngIf="reservation.clientPhone">tel: {{ reservation.clientPhone }}</div>
          <div *ngIf="reservation.clientEmail"><a href="mailto:{{ reservation.clientEmail }}">{{ reservation.clientEmail }}</a></div>
        </td>
        <td>
        	{{ reservation.price ?  reservation.price : ''  }}
        </td>
        <td>
        <select name="service" [(ngModel)]="reservation.status" (change)="statusChange(reservation, $event)">
        	<option *ngFor="let status of statuses" [ngValue]="status">  
        		{{status | translate}}
        	</option>
    	</select>
        </td>
    </tr>
  </table>
