<div class="schedule-wizard-progress-bar">
    <span class="schedule-wizard-progress-bar-point" [ngClass]="step == 1 ? 'active' : ''"><g:message code="wizard.generalSettings"/></span>
	<span class="schedule-wizard-progress-bar-point" [ngClass]="step == 2 ? 'active' : ''"><g:message code="wizard.selectWorkingDays"/></span>
	<span class="schedule-wizard-progress-bar-point" [ngClass]="step == 3 ? 'active' : ''"><g:message code="wizard.selectWorkingHours"/></span>
	<span class="schedule-wizard-progress-bar-point" [ngClass]="step == 4 ? 'active' : ''"><g:message code="wizard.selectServices"/></span>
	<span class="schedule-wizard-progress-bar-point" [ngClass]="step == 5 ? 'active' : ''"><g:message code="preview"/></span>
</div>

<div *ngIf="step == 1" >

  <%--
  <div *ngIf="!place_id" >
  	<strong><g:message code="place"/>:</strong>
  	<div class="schedule-wizard-row">
  		<span class="schedule-wizard-label"><g:message code="title"/>:</span>
  		<input [(ngModel)]="newPlaceTitle" type="text" />
  	</div>
  	<div class="schedule-wizard-row">
  		<span class="schedule-wizard-label"><g:message code="address"/>:</span>
  		<input [(ngModel)]="newPlaceAddress" type="text" />
  	</div>
  	<div class="schedule-wizard-row">
  		<span class="schedule-wizard-label"><g:message code="phone"/>:</span>
  		<input [(ngModel)]="newPlacePhoneNumber" type="text" />
  	</div>
  </div>
  --%>
  

  <strong><g:message code="wizard.generalSettings"/>:</strong>
  <div class="schedule-wizard-row">
  	<span class="schedule-wizard-label"><g:message code="title"/>:</span>
  	<input [(ngModel)]="scheduleTitle" type="text" />
  </div>

  <div class="schedule-wizard-row">
	<label>
		<input type="checkbox" name="confidential" value="1" [(ngModel)]="scheduleConfidential" />
       	<g:message code="private"/>
    </label>
  </div>

  <div class="schedule-wizard-row">
	<label>
		<input type="checkbox" name="readOnly" value="1" [(ngModel)]="scheduleReadOnly" />
       	<g:message code="readOnly"/>
    </label>
  </div>

  <div class="schedule-wizard-row">
  	<g:message code="wizard.allowMultiBooking"/>?
  	<label>
  		<input type="radio" name="multiBooking" [(ngModel)]="multiBooking" [value]="true" >
  		<g:message code="yes"/>
  	</label>
	<label>
		<input type="radio" name="multiBooking" [(ngModel)]="multiBooking" [value]="false">
		<g:message code="no"/>
	</label>
  </div>

  <div *ngIf="multiBooking" class="schedule-wizard-row">
  	<span class="schedule-wizard-label">
		<g:message code="wizard.multiBookingMaxNumber"/>:
	</span>
	<input [(ngModel)]="multiBookingMaxNumber" type="number" min="1" size="3"   />   

  </div>

  <a href="#" (click)="(scheduleTitle) && goStep(2, $event)" [class.disabled]="!(scheduleTitle )" class="schedule-wizard-next-btn" >
  	<g:message code="wizard.next"/>
  </a>	
</div>
  

<div *ngIf="step == 2" >   

  <strong><g:message code="wizard.plsSelectWorkingDays"/>:</strong>
  <div class="schedule-wizard-row">
  	<div *ngFor="let workingDay of workingDays">
                <label>
                    <input type="checkbox"
                           name="days"
                           value="{{ workingDay.dayIndex }}"
                           [(ngModel)]="workingDay.checked"
                           (change)="updateWorkingDays(workingDay, $event)"
                           />
                    {{workingDay.name | translate}}
                </label>
    </div>

  </div>

  <a href="javascript:;" (click)="goStep(1, $event)" class="schedule-wizard-prev-btn">
		<g:message code="wizard.back"/>
	</a>

  <a href="#" (click)="isWorkingDaySelected() && goStep(3, $event)" [class.disabled]="!isWorkingDaySelected()" class="schedule-wizard-next-btn" >
  	<g:message code="wizard.next"/>
  </a>
</div>	

<div *ngIf="step == 3" >

	<div class="schedule-wizard-float-sections clra">
		<div class="schedule-wizard-section">
			<div class="schedule-wizard-caption"><g:message code="workingHours"/></div>
			<div class="schedule-wizard-row">
				<span class="schedule-wizard-label">
					<g:message code="period"/>:
				</span>
				<input [(ngModel)]="timeSpec.timeRange.startHour" type="number" min="0" [max]="timeSpec.timeRange.endHour" size="2" (change)="changeDetails(null, $event)"  /> :
				<input [(ngModel)]="timeSpec.timeRange.startMinute" type="number" min="0" max="59" step="10" size="2" (change)="changeDetails(null, $event)" />
				-
				<input [(ngModel)]="timeSpec.timeRange.endHour" type="number" [min]="timeSpec.timeRange.startHour" max="23" size="2" (change)="changeDetails(null, $event)"  /> :
				<input [(ngModel)]="timeSpec.timeRange.endMinute" type="number" min="0" max="59" step="10" size="2" (change)="changeDetails(null,  $event)"  />
			</div>

			<div class="schedule-wizard-row">
				<span class="schedule-wizard-label"><g:message code="timeStep"/> :</span>
				<!-- max time step 24 hours -->
				<input [(ngModel)]="timeSpec.timeStep" type="number" min="0" step="5" size="2" max="1440" /> <g:message code="minutes"/> 
			</div>

			<div class="schedule-wizard-row">
				<span class="schedule-wizard-label"><g:message code="breakStep"/> :</span>
				<!-- max time step 24 hours -->
				<input [(ngModel)]="timeSpec.breakStep" type="number" min="0" step="5" size="2" max="1440" /> <g:message code="minutes"/> 
			</div>

			
		</div>	

		<div class="schedule-wizard-section">
			<div class="schedule-wizard-row schedule-wizard-row-gap schedule-wizard-row-no-in">
				<span class="schedule-wizard-caption"><g:message code="wizard.doYouHaveBreaks"/>?</span>
				<label><input type="radio" name="hasBreaks" [(ngModel)]="timeSpec.hasBreaks" [value]="true" (change)="changeBreakStatus(null,  $event)" ><g:message code="yes"/></label>
				<label><input type="radio" name="hasBreaks" [(ngModel)]="timeSpec.hasBreaks" [value]="false" (change)="changeBreakStatus(null,  $event)"><g:message code="no"/></label>
			</div>
			<div *ngIf="timeSpec.hasBreaks" >
				<div class="schedule-wizard-row">
					
					
					<div *ngFor="let timeBreak of timeSpec.breaks">
						<span class="schedule-wizard-label"><g:message code="period"/>:</span>
						<input [(ngModel)]="timeBreak.startHour" type="number" min="0" [max]="timeBreak.endHour" size="2" (change)="changeDetails(null, $event)"  /> :
						<input [(ngModel)]="timeBreak.startMinute" type="number" min="0" max="59" step="10" size="2" (change)="changeDetails(null, $event)" />
						-
						<input [(ngModel)]="timeBreak.endHour" type="number" [min]="timeBreak.startHour" max="23" size="2" (change)="changeDetails(null, $event)"  /> :
						<input [(ngModel)]="timeBreak.endMinute" type="number" min="0" max="59" step="10" size="2" (change)="changeDetails(null,  $event)"  />

						<a href="javascript:;" (click)="removeTimeBreak(timeSpec, timeBreak)">
							<g:message code="wizard.remove"/>
						</a>
					</div>
					<a href="javascript:;" (click)="addTimeBreak(timeSpec)"><g:message code="add"/></a>
					
				</div>
			</div>
		</div>	
	</div>

	<div class="schedule-wizard-row schedule-wizard-row-gap schedule-wizard-row-no-in">
		<span class="schedule-wizard-caption"><g:message code="wizard.doYouHaveDaysWithDiffTime"/>?</span>
		<label><input type="radio" name="otherDaysSchedule" [(ngModel)]="otherDaysSchedule" [value]="true"><g:message code="yes"/></label>
		<label><input type="radio" name="otherDaysSchedule" [(ngModel)]="otherDaysSchedule" [value]="false"><g:message code="no"/></label>
	</div>

	<div *ngIf="otherDaysSchedule" >
		<div *ngFor="let workingDay of workingDays">
			<div class="schedule-wizard-days-sections clra" *ngIf="workingDay.checked" [ngClass]="workingDay.customWorkingHours ? 'selected' : ''">
				<div class="schedule-wizard-day-name-section">
					<label><input type="checkbox" [(ngModel)]="workingDay.customWorkingHours" />  {{workingDay.name | translate}}</label>
				</div>

				<div *ngIf="workingDay.customWorkingHours" class="schedule-wizard-section schedule-wizard-section-in">
					<div class="schedule-wizard-caption"><g:message code="workingHours"/>({{workingDay.name | translate}})</div>
					<div class="schedule-wizard-row">
						<span class="schedule-wizard-label"><g:message code="period"/>:</span>
						<input [(ngModel)]="workingDay.timeSpec.timeRange.startHour" type="number" min="0" [max]="workingDay.timeSpec.timeRange.endHour" size="2" (change)="changeDetails(workingDay,  $event)"  /> :
						<input [(ngModel)]="workingDay.timeSpec.timeRange.startMinute" type="number" min="0" max="59" step="10" size="2" (change)="changeDetails(workingDay,  $event)"  />
						-
						<input [(ngModel)]="workingDay.timeSpec.timeRange.endHour" type="number" [min]="workingDay.timeSpec.timeRange.startHour" max="23" size="2" (change)="changeDetails(workingDay,  $event)"  /> :
						<input [(ngModel)]="workingDay.timeSpec.timeRange.endMinute" type="number" min="0" max="59" step="10" size="2" (change)="changeDetails(workingDay,  $event)"  />
					</div>
				</div>	

				<div *ngIf="workingDay.customWorkingHours" class="schedule-wizard-section schedule-wizard-section-in">
					<div class="schedule-wizard-row schedule-wizard-row-gap schedule-wizard-row-no-in">
						<span class="schedule-wizard-caption"><g:message code="wizard.doYouHaveBreaks"/>({{workingDay.name | translate}})?</span>
						<label><input type="radio" name="hasBreaks{{workingDay.name}}" [(ngModel)]="workingDay.timeSpec.hasBreaks" [value]="true" (change)="changeBreakStatus(workingDay,$event)"><g:message code="yes"/></label>
						<label><input type="radio" name="hasBreaks{{workingDay.name}}" [(ngModel)]="workingDay.timeSpec.hasBreaks" [value]="false" (change)="changeBreakStatus(workingDay,  $event)"><g:message code="no"/></label>
					</div>
					<div *ngIf="workingDay.timeSpec.hasBreaks" >
						<div class="schedule-wizard-row">
							<div *ngFor="let timeBreak of workingDay.timeSpec.breaks">
								<span class="schedule-wizard-label"><g:message code="period"/>:</span>
								<input [(ngModel)]="timeBreak.startHour" type="number" min="0" [max]="timeBreak.endHour" size="2" (change)="changeDetails(workingDay, $event)"  /> :
								<input [(ngModel)]="timeBreak.startMinute" type="number" min="0" max="59" step="10" size="2" (change)="changeDetails(workingDay, $event)" />
								-
								<input [(ngModel)]="timeBreak.endHour" type="number" [min]="timeBreak.startHour" max="23" size="2" (change)="changeDetails(workingDay, $event)"  /> :
								<input [(ngModel)]="timeBreak.endMinute" type="number" min="0" max="59" step="10" size="2" (change)="changeDetails(workingDay,  $event)"  />

								<a href="javascript:;" (click)="removeTimeBreak(workingDay.timeSpec, timeBreak)">
									<g:message code="wizard.remove"/>
								</a>
							</div>
							<a href="javascript:;" (click)="addTimeBreak(workingDay.timeSpec)"><g:message code="add"/></a>
						</div>
					</div>
				</div>


			</div>
		</div>

	</div>

	<a href="javascript:;" (click)="goStep(2, $event)" class="schedule-wizard-prev-btn">
		<g:message code="wizard.back"/>
	</a>
	<a href="javascript:;" (click)="goStep(4, $event)" class="schedule-wizard-next-btn">
		<g:message code="wizard.next"/>
	</a>

	
</div>	

<div *ngIf="step == 4" >
  <strong><g:message code="wizard.pleaseAddServices"/>:</strong>
  <div *ngIf="services" class="schedule-wizard-row" >
  	<div *ngFor="let service of services" class="schedule-wizard-service-row">
        <g:message code="title"/>: <input type="text" [(ngModel)]="service.name" placeholder="Your service name" [class.invalid]="!service.name" />
        <%-- 
        <g:message code="price"/>: $<span>{{ service.price }}</span>
        <input type="number" min="0" step="0.5" [(ngModel)]="service.price" />--%>
        <g:message code="duration"/>: <input type="number" min="0" step="1" [(ngModel)]="service.duration" />
        |
        <a href="javascript:;" (click)="removeService(service)">
        	<g:message code="wizard.remove"/>
        </a>
        |
        <a href="javascript:;" (click)="adjustServicePrice(service)">
        	<g:message code="wizard.adjustPrice"/>
        </a>
    </div>
    <div>
    	<a href="javascript:;" (click)="addService()"><g:message code="wizard.addService"/></a>
    </div>

  </div>

	<a href="javascript:;" (click)="goStep(3, $event)" class="schedule-wizard-prev-btn">
		<g:message code="wizard.back"/>
	</a>
	<a href="javascript:;" (click)="isPreviewAllowed() && goPreview($event)" class="schedule-wizard-next-btn" [class.disabled]="!isPreviewAllowed()" >
		<g:message code="wizard.next"/>
	</a>
</div>

<div *ngIf="step == 5" >
  <strong><g:message code="wizard.previewSchedule"/>:</strong>
  <div>
    <schedule_edit (timeBlocksUpdated)="onScheduleTimeBlocksUpdated($event)" [data_import]="dataForExport" [mode]="'edit'" [place_id]="place_id"></schedule_edit>
  </div>
  

  <a href="javascript:;" (click)="goStep(4, $event)" class="schedule-wizard-prev-btn">
  	<g:message code="wizard.back"/>
  </a>
  <a href="javascript:;" (click)="save()" class="schedule-wizard-save-btn"><g:message code="save"/></a>
</div>



<!-- popups -->
<div *ngIf="showAdjustServiceModal" class="control-modal-overlay" (click)="adjustPriceOverlayClick($event)">
	<div class="control-modal" *ngIf="currentService">
		<div>
			<g:message code="basePrice"/>: $<input type="number" min="0" step="0.5" [(ngModel)]="currentService.price" />
		</div>

		<div *ngFor="let timeRangePrice of currentService.timeRangePrices" class="schedule-wizard-time-range-price-block">
			<div>
				<g:message code="wizard.forTimePeriod"/>:
				
				<input [(ngModel)]="timeRangePrice.timeRange.startHour" type="number" min="0" [max]="timeSpec.timeRange.endHour" size="2"   /> :
				<input [(ngModel)]="timeRangePrice.timeRange.startMinute" type="number" min="0" max="59" step="10" size="2"  />
				-
				<input [(ngModel)]="timeRangePrice.timeRange.endHour" type="number" [min]="timeSpec.timeRange.startHour" max="23" size="2"  /> :
				<input [(ngModel)]="timeRangePrice.timeRange.endMinute" type="number" min="0" max="59" step="10" size="2"  />

			</div>
			<div class="schedule-wizard-time-range-price-days">
				<div *ngFor="let workingDay of workingDays" >
					<div class="schedule-wizard-day-name-section">
						<label><input type="checkbox" [(ngModel)]="timeRangePrice.days[workingDay.dayIndex]" />  {{workingDay.name | translate}}</label>
					</div>
				</div>
				
			</div>
			<g:message code="price"/>: $<input type="number" min="0" step="0.5" [(ngModel)]="timeRangePrice.price" />
		</div>
		

		<div>
			<a href="javascript:;" (click)="addServiceTimeRangePrice(currentService)">
				<g:message code="wizard.addRangePrice"/>
			</a>
		</div>

	</div>
</div>
  