// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require jquery-2.2.0.min
//= require bootstrap
///= require_tree .
//= require_self

if (typeof jQuery !== 'undefined') {
	
    (function($) {
    	jQuery(document).ready(whenDeskInitialize);
        $(document).ajaxStart(function() {
            $('#spinner').fadeIn();
        }).ajaxStop(function() {
            $('#spinner').fadeOut();
        });
    })(jQuery);
}

function whenDeskInitialize(){
  initLangeChanger();
}

function initLangeChanger(){
	jQuery('#lang-select').change(function(){
		var lang = jQuery(this).val();
		var url = jQuery(this).attr('data-url');
		jQuery.ajax({
			url: url, 
			data: { lang : lang }, 
			success: function() {
				location.reload();	
			}

		});
	});
}
