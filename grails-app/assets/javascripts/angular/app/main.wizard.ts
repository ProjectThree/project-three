import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { WizardModule }  from './wizard.module';

platformBrowserDynamic().bootstrapModule(WizardModule);