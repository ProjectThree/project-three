export class WdTimeSpec {  
    
  timeRange:WdTimeRange;
  timeStep:int = 30;
  breakStep:int = 0;

  hasBreaks:boolean = false;

  breaks:Array<WdTimeRange>;
  /*
  lunchStartHour:int = 12;
  lunchStartMinute:int = 0;
  lunchEndHour:int = 13;
  lunchEndMinute:int = 0;
  */

    
  constructor() {
    this.timeRange = new WdTimeRange();   
    this.breaks = [];
  }

  importFromData(data){
    this.timeStep = data.timeStep; 
    this.breakStep = data.breakStep; 
    this.hasBreaks = data.hasBreaks ? true : false;
    this.timeRange = new WdTimeRange();
    this.timeRange.importFromData(data.timeRange);

    this.breaks = [];
    if(data.breaks && data.breaks.length > 0){
      for(let dataBreak of data.breaks){
        let breakTimeRange = new WdTimeRange();
        breakTimeRange.importFromData(dataBreak);
        this.breaks.push(breakTimeRange);

      }
    }

  }

  initBreaks(){
    if(this.hasBreaks){
       if(!this.breaks || this.breaks.length <= 0){
          this.addBreak(); 
       }
    }
    else{
      this.clearBreaks();
    }
  }

  clearBreaks(){
    this.breaks = []; 
  }

  addBreak(startHour:int = 12, startMinute: int = 0, endHour: int = 13, endMinute: int = 0){
    var timeRange:WdTimeRange = new WdTimeRange(startHour, startMinute, endHour, endMinute);
    this.breaks.push(timeRange);
  }

  addBreakRange(timeRange:WdTimeRange){
    this.breaks.push(timeRange);
  }

  removeBreakRange(timeRange){
    var index = this.breaks.indexOf(timeRange, 0);
    if (index > -1) {
      this.breaks.splice(index, 1);
    }
  }

  copyTo(distTimeSpec:WdTimeSpec){
    this.timeRange.copyTo( distTimeSpec.timeRange) ;
    distTimeSpec.timeStep = this.timeStep;
    distTimeSpec.breakStep = this.breakStep;
    distTimeSpec.hasBreaks = this.hasBreaks;

    distTimeSpec.clearBreaks();
    for (let timeBreak of this.breaks) {
      var newTimeBreak:WdTimeRange = new WdTimeRange();
       timeBreak.copyTo(newTimeBreak);
       distTimeSpec.addBreakRange(newTimeBreak);
    }
  }

  clone(){
    let newWdTime = new WdTimeSpec();
    this.copyTo(newWdTime);
    return newWdTime;
  }
}  


export class WdTimeRange { 
  startHour:int = 8;
  startMinute:int = 0;
  endHour:int = 17;
  endMinute:int = 0;
  
  constructor(startHour:int = 8, startMinute: int = 0, endHour: int = 17, endMinute: int = 0) {
    this.startHour = startHour;
    this.startMinute = startMinute;
    this.endHour = endHour;
    this.endMinute = endMinute;
  }

  containsTime(time:int){
    if(time >= this.getFromTime() && time<= this.getEndTime() ){
      return true;
    }

    return false;
  }

  importFromData(data):WdTimeRange{
    
    this.startHour = data.startHour;
    this.startMinute = data.startMinute;
    this.endHour = data.endHour;
    this.endMinute = data.endMinute;
    return this;
  }

  getStartTimeLabel():String{
    return this.pad(this.startHour, 2) + ":" +  this.pad(this.startMinute, 2);
  }

  getEndTimeLabel():String{
    return this.pad(this.endHour, 2) + ":" +  this.pad(this.endMinute, 2);
  }

  getPeriodLabel():String{
    return this.getStartTimeLabel() + '-' + this.getEndTimeLabel();
  }

  public pad(str, size):String {
    let s = String(str);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
  }

  getFromTime():int{
    let fromTime = this.startHour * 60; 
    fromTime += this.startMinute;
    return fromTime;
  }

  getEndTime():int{
    let endTime = this.endHour * 60; 
    endTime += this.endMinute;
    return endTime;
  }

  getDuration():int{
    return this.getEndTime() - this.getFromTime();
  }

  copyTo(distTimeRange:WdTimeRange){
    distTimeRange.startHour = this.startHour;
    distTimeRange.startMinute = this.startMinute;
    distTimeRange.endHour = this.endHour;
    distTimeRange.endMinute = this.endMinute;
  }

  clone(){
    let newTimeRange = new WdTimeRange();
    this.copyTo(newTimeRange);
    return newTimeRange;
  }

}  