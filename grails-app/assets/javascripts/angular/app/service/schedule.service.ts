import { Injectable } from '@angular/core';
import { Http, Response, HTTP_PROVIDERS, Headers, RequestOptions, URLSearchParams  } from '@angular/http';
import { WdTimeSpec, WdTimeRange } from '../when_desk/WdTime.ts';

@Injectable()
export class ScheduleService {
	constructor(private http: Http){}

	getCurrentDate(){
		return this.http.get(window.wdConfig.routes.currentDate);
	}

	dispatchDataTo(data_import, object, isWizardMode = false){
		object.scheduleTitle = data_import.title;
    	object.scheduleConfidential = data_import.confidential ? true : false;
    	object.scheduleReadOnly = data_import.readOnly ? true : false;
    	
    	object.multiBookingMaxNumber = data_import.multiBookingMaxNumber > 1 ? data_import.multiBookingMaxNumber : 1;
		object.multiBooking = data_import.multiBooking ? true : false;
		object.otherDaysSchedule = data_import.otherDaysSchedule ? true : false;

		if(data_import.timeBlocks && data_import.timeBlocks.length > 0 ){
      		object.timeBlocks = [];
      		for(let timeBlock of data_import.timeBlocks){
        		let newTimeBlock = {};
        		newTimeBlock.dayIndex = timeBlock.dayIndex;
        		//newTimeBlock.title = timeBlock.title;
        		//newTimeBlock.multiBookingMaxNumber = timeBlock.multiBookingMaxNumber;
        		newTimeBlock.options = timeBlock.options;
        
        		newTimeBlock.timeRange = new WdTimeRange(timeBlock.timeRange.startHour, timeBlock.timeRange.startMinute, timeBlock.timeRange.endHour, timeBlock.timeRange.endMinute);
        		console.log(newTimeBlock.timeRange);
        		object.timeBlocks.push(newTimeBlock);

      		}
      	}

      	object.services = data_import.services;

      	object.timeSpec = new WdTimeSpec();
      	object.timeSpec.importFromData(data_import.timeSpec);

      	if(isWizardMode){
      		object.workingDays = [
    			{name:'Monday',   	dayIndex:2, checked:false },
    			{name:'Tuesday',	dayIndex:3, checked:false },
    			{name:'Wednesday',  dayIndex:4, checked:false },
    			{name:'Thursday',   dayIndex:5, checked:false },
    			{name:'Friday',     dayIndex:6, checked:false },
    			{name:'Saturday',   dayIndex:7, checked:false },
    			{name:'Sunday',     dayIndex:1, checked:false }
  			]

  			for(let importWorkingDay of data_import.workingDays){
  				for(let workingDay of object.workingDays){
  					if(workingDay.dayIndex == importWorkingDay.dayIndex){
  						workingDay.checked = true;

  						if(importWorkingDay.timeSpec){
  							workingDay.assigned = true;
  							workingDay.customWorkingHours = true;
  							workingDay.timeSpec = new WdTimeSpec();
      						workingDay.timeSpec.importFromData(importWorkingDay.timeSpec);
  						}

  						break;
  					}
  				}

  			}

      	}

      	
	}


	addReservation(sendData){
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		let body = JSON.stringify(sendData);

    	return this.http.post( window.wdConfig.routes.reservationAddJson, body, options)
	}

	getScheduleJson(scheduleId){
		let params: URLSearchParams = new URLSearchParams();
		
 		params.set('id', scheduleId);
 		params.set('config', true);
		return this.http.get( window.wdConfig.routes.scheduleJson, 
    				{
    					search: params
 					}
 				);
	}


	getReservations(scheduleId){
		let params: URLSearchParams = new URLSearchParams();
 		params.set('output', 'json');
 		params.set('schedule_id', scheduleId);
		return this.http.get( window.wdConfig.routes.scheduleReservations, 
					{
    					search: params
					
 					}
 				);
	}


	getSchedules(place_id){
		let params: URLSearchParams = new URLSearchParams();
 		params.set('output', 'json');
 		if(place_id){
 			params.set('place_id', place_id);
 		}
 		
		return this.http.get( window.wdConfig.routes.schedules, 
					{
    					search: params
					
 					}
 				);
	}

	

	saveSchedule(sendData){
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		let body = JSON.stringify(sendData);

    	return this.http.post( window.wdConfig.routes.scheduleSaveJson, body, options)
	}
	
}