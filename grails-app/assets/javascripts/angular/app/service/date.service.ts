import { Injectable } from '@angular/core';
import { Http, Response, HTTP_PROVIDERS } from '@angular/http';

@Injectable()
export class DateService {
	constructor(private http: Http){}

	getCurrentDate(){
		return this.http.get(window.wdConfig.routes.currentDate);
	}
}