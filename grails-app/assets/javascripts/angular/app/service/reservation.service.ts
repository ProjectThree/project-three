import { Injectable } from '@angular/core';
import { Http, Response, HTTP_PROVIDERS, Headers, RequestOptions, URLSearchParams  } from '@angular/http';

@Injectable()
export class ReservationService {
	constructor(private http: Http){}

	/*
	addReservation(sendData){
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		let body = JSON.stringify(sendData);

    	return this.http.post( window.wdConfig.routes.reservationAddJson, body, options)
	}

	saveSchedule(sendData){
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		let body = JSON.stringify(sendData);

    	return this.http.post( window.wdConfig.routes.scheduleSaveJson, body, options)
	}
	*/

	updateStatus(reservationId, status){
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		let body = JSON.stringify({status: status, id: reservationId  });
		
    	return this.http.post( window.wdConfig.routes.updateReservationStatus, body, options)
	}

	getReservations(){
		let params: URLSearchParams = new URLSearchParams();
 		params.set('output', 'json');
		return this.http.get( window.wdConfig.routes.reservations, 
					{
    					search: params
					
 					}
 				);
	}

	

	
	
}