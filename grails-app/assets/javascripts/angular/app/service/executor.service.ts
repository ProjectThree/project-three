import { Injectable } from '@angular/core';
import { Http, Response, HTTP_PROVIDERS, Headers, RequestOptions, URLSearchParams  } from '@angular/http';

@Injectable()
export class ExecutorService {
	constructor(private http: Http){}

	
  saveExecutor(sendData){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(sendData);

    return this.http.post( window.wdConfig.routes.executorSaveJson, body, options)
  }

	getExecutors(scheduleId){
		let params: URLSearchParams = new URLSearchParams();
 		params.set('output', 'json');
		return this.http.get( window.wdConfig.routes.executors, 
					{
    					search: params
					
 					}
 				);
	}

	
	
}