import { Component, Input, ElementRef} from '@angular/core';
import { Http, Response, HTTP_PROVIDERS, Headers, RequestOptions } from '@angular/http';
import { WdTimeSpec, WdTimeRange } from './when_desk/WdTime.ts';
import { DateService } from './service/date.service.ts';
import { ScheduleService } from './service/schedule.service.ts';
import { TranslateService } from './translate/index.ts';

@Component({
  selector: 'schedule',
  templateUrl: '/templates/scheduleRender',
  providers: [DateService, ScheduleService]
})
export class ScheduleRenderComponent implements AfterViewInit {
	componentName: 'ScheduleRenderComponent';
	@Input() data_import;
  @Input() place_id;
  @Input() mode;

  reservations = [];
  timeBlocks = [];
  currentReservation = null;
  showReservationModal:Boolean = false;
  showManageTimeBlockModal:Boolean = false;

  currentReservationPrice = null;
  currentBookingTimeBlock = null;
  schedule_id:int;
  daysOffset = 0;
  daysOffsetLimit = 21; // max 4 weeks to show
  daysToShow = 7;
  multiBookingMaxNumber:int = 1;
  multiBooking:boolean = false;

  constructor(
    private dateService: DateService, 
    private scheduleService: ScheduleService, 
    private http: Http, 
    private _elementRef:ElementRef,
    private _translate: TranslateService
    )
  {

    this.setInterfaceLanguage();
    //this.translatedText = this._translate.instant('Monday');

    dateService.getCurrentDate().subscribe(
      res => this.currentDate = new Date(res.text() ),
      err => console.log(err),
      ()=> setTimeout(_=> this.inflate()) 

    );

    let native = this._elementRef.nativeElement;
    this.schedule_id = native.getAttribute('data-id');
    if(this.schedule_id){
      this.loadScheduleData()
    }

  }

  setInterfaceLanguage(){
    this._translate.use(document['locale'] || 'en');
  }

  loadScheduleData(){

    this.scheduleService.getScheduleJson(this.schedule_id).subscribe(res => this.data_import = res.json(),
      err => console.log(err),
      ()=> {
        this.inflate();
        this.refreshReservations(); 
      }
    );

  }

  ngAfterViewInit() {
    setTimeout(_=> this.inflate()); 
  }

  inflate(): void{
    if(this.data_import){
      this.buildFromDataImport(this.data_import);    		
    }

  }

  refreshReservations():void{
    if(this.schedule_id){
      this.scheduleService.getReservations(this.schedule_id).subscribe(res => this.serverReservations = res.json(),
        err => console.log(err),
        ()=> {
          
          if(this.serverReservations){
            for(let reservation of this.serverReservations){
                reservation.startTime = new Date(reservation.dateReserved);
                let sessionDurationMills = reservation.sessionDuration * 60 * 1000;
                reservation.endTime = new Date(reservation.startTime.getTime() + sessionDurationMills); 
            }
            this.markedReservedTimes();
          }
          
        }
      );
    }
  }

  jumpWeek(direction):void{
    this.daysOffset += direction * this.daysToShow;
    if(this.daysOffset < 0){
      this.daysOffset = 0;
    }

    if(this.daysOffset >= this.daysOffsetLimit){
      this.daysOffset = this.daysOffsetLimit;
    }
    
    this.inflate();
  }

  buildFromDataImport(data_import):void{
    //console.log(data_import);
    if(!this.currentDate && data_import.currentDate){
      this.currentDate = data_import.currentDate 
    }

    if(!this.currentDate){
      return;
    }

    let dayDateTime = this.currentDate.getTime();
    if(this.daysOffset > 0 ){
      dayDateTime += this.daysOffset * 3600 * 24 * 1000;
    }

    let dayDate = new Date(dayDateTime); 
    let currentDayIndex =  dayDate.getDay() + 1;
    if(this.mode == 'edit'){
      currentDayIndex = 2;
    }
    // stupid fix
    let realDayIndex = currentDayIndex;
    if(realDayIndex == 1){
      realDayIndex = 8;
    }


    let orderedDays = [];
    let orderedDaysOutIndex = [];
    for (let workingDay of data_import.workingDays) {
      // second part of stupid fix
      if(workingDay.dayIndex == 1 && (currentDayIndex == workingDay.dayIndex || currentDayIndex <= 8 ) ){
        orderedDays.push(workingDay);
      }
      else if(workingDay.dayIndex >= realDayIndex){
        orderedDays.push(workingDay);
      }
      else{
        orderedDaysOutIndex.push(workingDay);
      }
    }

    orderedDays = orderedDays.concat(orderedDaysOutIndex);
    this.workingDays = [];
    //for (let workingDay of orderedDays) {
    let orderedDaysCount = orderedDays.length;
    //this.daysToShow= orderedDaysCount;
    for(let workindDayLoopIndex = 0; workindDayLoopIndex < this.daysToShow; workindDayLoopIndex++){
      
      let adjustedWorkindDayLoopIndex = workindDayLoopIndex - Math.floor(workindDayLoopIndex / orderedDaysCount) * orderedDaysCount
      let workingDay = orderedDays[adjustedWorkindDayLoopIndex];
      let newWorkingDay = { name: workingDay.name, dayIndex : workingDay.dayIndex };

      let currentDayDate = new Date(dayDate.getTime()); 
      newWorkingDay.date = currentDayDate;
      dayDate = new Date(dayDate.getTime() + 3600 * 24 * 1000); 


      var dayTimeSpec = data_import.timeSpec; 
      if(workingDay.timeSpec){
        dayTimeSpec = workingDay.timeSpec;
      }

      var dayTimeRangeSource = dayTimeSpec.timeRange;

      let dayTimeRange:WdTimeRange = new WdTimeRange(dayTimeRangeSource.startHour, dayTimeRangeSource.startMinute, dayTimeRangeSource.endHour, dayTimeRangeSource.endMinute);

      let dayTimes = [];

      let dayBreaks = []
      if(dayTimeSpec.hasBreaks && dayTimeSpec.breaks.length > 0 ){
        for(let dayBreak of dayTimeSpec.breaks){
          let dayBreakRange:WdTimeRange = new WdTimeRange(dayBreak.startHour, dayBreak.startMinute, dayBreak.endHour, dayBreak.endMinute);	
          dayBreaks.push(dayBreakRange);
        }
      }

      let dayTimePosition = 0;
      for (let currentTime:int = dayTimeRange.getFromTime(); currentTime <= dayTimeRange.getEndTime(); currentTime+= dayTimeSpec.timeStep ) {
        var timeInfo = this.makeDayTime(currentTime, currentDayDate);
        timeInfo.dayTimePosition = dayTimePosition++;
        timeInfo.parentDay = newWorkingDay;
        dayTimes.push( timeInfo );

        if(dayTimeSpec.hasBreaks && dayTimeSpec.breaks.length > 0 ){
          for(let dayBreak of dayBreaks){
            if(currentTime >=  dayBreak.getFromTime() && currentTime <=  dayBreak.getEndTime() ){
              timeInfo.beforeBreak = true;
              currentTime = dayBreak.getEndTime() ;
              var timeInfo = this.makeDayTime(currentTime, currentDayDate);
              timeInfo.dayTimePosition = dayTimePosition++;
              timeInfo.parentDay = newWorkingDay;
              timeInfo.afterBreak = true;
              dayTimes.push( timeInfo );
            }
          }
        }

        if(dayTimeSpec.breakStep > 0){
          currentTime += dayTimeSpec.breakStep;
        }

      }

      newWorkingDay.times = dayTimes;

      this.workingDays.push(newWorkingDay);

      let sourceServices = data_import.services;

      for(let sourceService of sourceServices ){
        if(sourceService.timeRangePrices && sourceService.timeRangePrices.length > 0){
          for(let timeRangePrice of sourceService.timeRangePrices ){
            let timeRange:WdTimeRange = new WdTimeRange(timeRangePrice.timeRange.startHour, timeRangePrice.timeRange.startMinute, timeRangePrice.timeRange.endHour, timeRangePrice.timeRange.endMinute);    
            timeRangePrice.timeRange = timeRange;
          }
        }
      }

      this.services = sourceServices;

      
      if(data_import.multiBooking){
        this.multiBooking = data_import.multiBooking;
        if(data_import.multiBookingMaxNumber > 1){
          this.multiBookingMaxNumber = data_import.multiBookingMaxNumber; 
        }
      }

    }

    if(data_import.timeBlocks && data_import.timeBlocks.length > 0 ){
      this.timeBlocks = [];
      for(let timeBlock of data_import.timeBlocks){
        let newTimeBlock = {};
        newTimeBlock.dayIndex = timeBlock.dayIndex;
        newTimeBlock.options = timeBlock.options;
        /*newTimeBlock.title = timeBlock.title;
        if(timeBlock.multiBookingMaxNumber > 1){
          newTimeBlock.multiBookingMaxNumber = timeBlock.multiBookingMaxNumber;
        }*/
        
        newTimeBlock.timeRange = new WdTimeRange(timeBlock.timeRange.startHour, timeBlock.timeRange.startMinute, timeBlock.timeRange.endHour, timeBlock.timeRange.endMinute);
        this.timeBlocks.push(newTimeBlock);

      }

      this.renderTimeBlocks();



    }

    

    this.markedReservedTimes();
  }

  public switchEditMode():void{
    if(this.mode == 'edit'){
      this.mode = 'preview';
    }
    else{
      this.mode = 'edit'; 
    }

  }

  public renderTimeBlocks(){
    
    
    for(let workingDay of this.workingDays){
      let timeBlockIndex = 0;
      let lastDayTime = null;
      //let lastTimeBlock = null;
      for(let dayTime of workingDay.times){
        dayTime.timeBlock = null;
        for(let timeBlock of this.timeBlocks){
          if(workingDay.dayIndex == timeBlock.dayIndex && timeBlock.timeRange.containsTime(dayTime.time) ){
            //dayTime.timeBlock = timeBlock;
            
            if(lastDayTime && lastDayTime.timeBlock == timeBlock){
              lastDayTime.timeBlock = null;
              lastDayTime.timeBlockIndex = null;
            }
            else{
              timeBlockIndex = 0;
            }

            timeBlockIndex++;
            
            lastDayTime = dayTime;
            lastDayTime.timeBlock = timeBlock;
            lastDayTime.timeBlockIndex = timeBlockIndex;
            break;
            //lastTimeBlock = timeBlock;
          }

        }



      }
    }

  }

  markedReservedTimes(){
    
    let allReservations = this.reservations;
    if(this.serverReservations){
      allReservations = allReservations.concat(this.serverReservations);
    }

    console.log('all reservations');
    console.log(allReservations);



    
    for(let workingDay of this.workingDays){
      for(let dayTime of workingDay.times){

        

        // check with reservations
        dayTime.isReserved = false;
        for(let reservation of allReservations){
          if( dayTime.realDate >= reservation.startTime && dayTime.realTime <= reservation.endTime){
            
            //dayTime.isReserved += dayTime.isReserved ? dayTime.isReserved : 1;
            dayTime.reservedCount = dayTime.reservedCount ? dayTime.reservedCount + 1 : 1;
            /*var maxBookingNumber = 1;
            if(dayTime.timeBlock && dayTime.timeBlock.multiBookingMaxNumber > 1 ){
              maxBookingNumber = dayTime.timeBlock.multiBookingMaxNumber;
            }
            else if(this.multiBookingMaxNumber > 1){
              maxBookingNumber = this.multiBookingMaxNumber;
            }
            */
            /*
            console.log('------------');
            console.log(dayTime.timeBlock);
            */


            /*
            if(dayTime.timeBlock && dayTime.timeBlock.options){
              for(let timeBlockOption of dayTime.timeBlock.options){
                console.log('==========');
                console.log(timeBlockOption)
                if(timeBlockOption.multiBookingMaxNumber > 1) {
                    timeBlockOption.leftBookingCount = timeBlockOption.multiBookingMaxNumber - 0;//dayTime.reservedCount;
                    timeBlockOption.showLeftBookingCount = true;
                }
              }
            }
            */
            if(dayTime.timeBlock && dayTime.timeBlock.options){
              let reservationTimeBlockOptionIndex = reservation.timeBlockOptionIndex ? reservation.timeBlockOptionIndex : 0;
              let timeBlockOption = dayTime.timeBlock.options[reservationTimeBlockOptionIndex];
              timeBlockOption.reservedCount =  timeBlockOption.reservedCount ? timeBlockOption.reservedCount + 1 : 1; 
              if(timeBlockOption.reservedCount >= timeBlockOption.multiBookingMaxNumber ){
                timeBlockOption.isReserved = true;
              }

            }
            else if(this.multiBookingMaxNumber > 1){

                /*if(dayTime.timeBlock){
                  dayTime.timeBlock.leftBookingCount = this.multiBookingMaxNumber - dayTime.reservedCount;
                  dayTime.timeBlock.showLeftBookingCount = true;
                }
                */

               if(dayTime.reservedCount >= maxBookingNumber){
                 dayTime.isReserved = true;     
               }
            }
            else{
              dayTime.isReserved = true;  
            }

            
          }
        }


        if(dayTime.timeBlock && dayTime.timeBlock.options){
          for(let timeBlockOption of dayTime.timeBlock.options){
            if(timeBlockOption.multiBookingMaxNumber > 1) {
              let reservedCount = timeBlockOption.reservedCount ? timeBlockOption.reservedCount : 0;
              timeBlockOption.leftBookingCount = timeBlockOption.multiBookingMaxNumber - reservedCount;
              timeBlockOption.showLeftBookingCount = true;
            }
          }
        }

      }
    }

    
  }

  makeDayTime(currentTime, currentDayDate) {
    let currentHour:int = Math.floor( currentTime / 60 );
    let currentMinute:int = currentTime % 60;
    
    let timeStartDate = new Date(currentDayDate.getTime()); 
    timeStartDate.setHours(currentHour);
    timeStartDate.setMinutes(currentMinute);
    timeStartDate.setSeconds(0);
    timeStartDate.setMilliseconds(0)

    let timeInfo = { 
      'label' : this.pad(currentHour, 2) + ":" +  this.pad(currentMinute, 2), 
      'time' : currentTime, 
      'hour' : currentHour,
      'minute' : currentMinute,
      'realTime' : timeStartDate.getTime(),
      'realDate' : timeStartDate
    };
    return timeInfo;
  }

  pad(str, size):String {
    let s = String(str);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
  }

  public selectDay(workingDay, dayTime, event):void{
    
  }

  public manageTimeBlockClick(event):void{

  }


  public dayTimeClickWithBlock(workingDay, dayTime, timeBlock, optionIndex, event){
    if(dayTime.isReserved){
      return false
    }
    this.currentBookingTimeBlockOption = timeBlock.options[optionIndex];
    if(this.currentBookingTimeBlockOption.isReserved){
      return false;
    }
    this.dayTimeClick(workingDay, dayTime, event);    
    this.currentBookingTimeBlock = timeBlock;
    this.currentBookingTimeBlockOptionIndex = optionIndex;
    //this.currentBookingTimeBlockOption = timeBlock.options[optionIndex];

    if(this.currentBookingTimeBlockOption && this.currentBookingTimeBlockOption.multiBookingMaxNumber > 1 ){
      let maxBookingNumber = this.currentBookingTimeBlockOption.multiBookingMaxNumber;
      this.currentReservation.showLeftBookingCount = true;
      this.currentReservation.maxBookingNumber = maxBookingNumber;
      this.currentReservation.leftBookingCount =  maxBookingNumber - (this.currentBookingTimeBlockOption.reservedCount ? this.currentBookingTimeBlockOption.reservedCount : 0 );
    }
    

  }

  dayTimeClick(workingDay, dayTime, event){
    if(this.mode == 'edit'){
      this.selectDay(workingDay, dayTime, event);
      return;
    }

    if(dayTime.isReserved){
      return false
    }

    this.currentBookingTimeBlock = null;


    this.currentReservation = { day: workingDay, time: dayTime, clientInfo: {} };

    var maxBookingNumber = 1;
    /*if(dayTime.timeBlock && dayTime.timeBlock.multiBookingMaxNumber > 1 ){
      maxBookingNumber = dayTime.timeBlock.multiBookingMaxNumber;
    }
    else 
      */

    if(this.multiBookingMaxNumber > 1){
      maxBookingNumber = this.multiBookingMaxNumber;
    }

    if( maxBookingNumber > 1){
      this.currentReservation.showLeftBookingCount = true;
      this.currentReservation.maxBookingNumber = maxBookingNumber;
      this.currentReservation.leftBookingCount =  maxBookingNumber - (dayTime.reservedCount ? dayTime.reservedCount : 0 );
    }


    this.showReservationModal = true;

    if(this.services){
      if(this.services.length == 1){
        this.currentReservation.service = this.services[0];
        this.serviceChange();
      }
    }

    console.log(this.currentReservation);
  }

  public serviceChange(event){
    this.currentReservationPrice = 0;

    if(this.currentReservation.service){
      this.currentReservationPrice = this.currentReservation.service.price;
      if( this.currentReservation.service.timeRangePrices){
        
        let reservationTime = this.currentReservation.time.time;
        let reservationDayIndex = this.currentReservation.day.dayIndex;

        for(let timeRangePrice of this.currentReservation.service.timeRangePrices ){
          if(timeRangePrice.days[reservationDayIndex] && ( timeRangePrice.timeRange.getFromTime() <= reservationTime && reservationTime <= timeRangePrice.timeRange.getEndTime()  )  ){
            this.currentReservationPrice = timeRangePrice.price;
            break;
          }
          
        }
      }
    }
  }


  bookClick(event){
    console.log(this.currentReservation);
    let dayDate =  this.currentReservation.day.date;
    console.log(dayDate.toJSON());
    let reservationDateTime = dayDate.getFullYear() + '-' + this.pad(dayDate.getMonth() + 1, 2)  +'-'+  this.pad(dayDate.getDate(), 2) + ' ' + this.pad(this.currentReservation.time.hour, 2) + ':' + this.pad(this.currentReservation.time.minute, 2);

    let startTime = new Date(dayDate.getTime());  
    startTime.setHours(this.currentReservation.time.hour);
    startTime.setMinutes(this.currentReservation.time.minute);
    startTime.setSeconds(0);
    startTime.setMilliseconds(0)

    let sessionDurationMills = 0;
    let duration = 0;
    
    if(this.currentReservation.service && this.currentReservation.service.duration > 0){
      duration = this.currentReservation.service.duration;
      sessionDurationMills = duration * 60 * 1000;
    }

    if(this.currentBookingTimeBlock){
      reservationDateTime = dayDate.getFullYear() + '-' + this.pad(dayDate.getMonth() + 1, 2)  +'-'+  this.pad(dayDate.getDate(), 2) + ' ' + this.pad(this.currentBookingTimeBlock.timeRange.startHour, 2) + ':' + this.pad(this.currentBookingTimeBlock.timeRange.startMinute, 2);
      startTime.setHours(this.currentBookingTimeBlock.timeRange.startHour);
      startTime.setMinutes(this.currentBookingTimeBlock.timeRange.startMinute);  
      duration = this.currentBookingTimeBlock.timeRange.getDuration();
      sessionDurationMills = duration * 60 * 1000;
      
    }


    let endTime = new Date(startTime.getTime() + sessionDurationMills);  

    this.reservations.push({
      startTime: startTime,
      endTime: endTime
    });

    this.markedReservedTimes();

    console.log(this.reservations);



    //reservations
    if(this.schedule_id){
      let sendData = {
        time: reservationDateTime, 
        schedule_id: this.schedule_id, 
        duration: duration,
        serviceName : this.currentReservation.service.name,
        price: this.currentReservationPrice,
        clientInfo: this.currentReservation.clientInfo
      };

      if(this.currentBookingTimeBlock){
        sendData.timeBlockTitle = this.currentBookingTimeBlock.title;
      }

      
      sendData.timeBlockOptionIndex = this.currentBookingTimeBlockOptionIndex ? this.currentBookingTimeBlockOptionIndex : 0;
      

      if(this.currentReservation.maxBookingNumber > 1){
        sendData.maxBookingNumber = this.currentReservation.maxBookingNumber;
      }

      this.scheduleService.addReservation(sendData).subscribe(
        res => console.log(res),
        err => console.log(err),
        ()=> {
          alert('Thank you for booking!');
        }
      );	
    }
    


    this.showReservationModal = false;
  }

  modalOverlayClick(event){
    if(!event.target.className.match(/control-modal-overlay/i) ){
      return false;
    }

    return true;
  }

  reservationOverlayClick(event){
    if(event.target.className.match(/schedule-render-reservation-modal-overlay/i) ){
      this.showReservationModal = false;
      this.currentReservation = null;
      this.currentReservationPrice = null;
    }
  }

  getReservationFormattedPrice():String{
    if(this.currentReservation && this.currentReservation.service && this.currentReservationPrice){
      return '$' + this.currentReservationPrice.toFixed(2);
    }
    return null;
  }
}