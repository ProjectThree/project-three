import { Component, Input, Output, ElementRef, EventEmitter } from '@angular/core';
import { Http, Response, HTTP_PROVIDERS, Headers, RequestOptions } from '@angular/http';
import { WdTimeSpec, WdTimeRange } from './when_desk/WdTime.ts';
import { ScheduleRenderComponent } from './schedule.render.component';
import { DateService } from './service/date.service.ts';
import { ScheduleService } from './service/schedule.service.ts';
import { TranslateService } from './translate/index.ts';

@Component({
	selector: 'schedule_edit',
	templateUrl: '/templates/scheduleRender',
	providers: [DateService, ScheduleService]
})
export class ScheduleEditComponent extends ScheduleRenderComponent {
	componentName: 'ScheduleEditComponent';
	switchModeAvailable: Boolean = true;

	@Output() timeBlocksUpdated: EventEmitter<any> = new EventEmitter();

	public resetSelectedDayTimes():void{
		if(! ( this.selectedDayTimes && this.selectedDayTimes.length > 0 ) ){
			return;
		}

		for(let parentDayTime of this.selectedDayTimes[0].parentDay.times){
			parentDayTime.interimSelected = false;
		}

		for(let selectedDayTime of this.selectedDayTimes){
			selectedDayTime.active = false;
			selectedDayTime.selected = false;
			selectedDayTime.interimSelected = false;
		}

		this.selectedDayTimes = [];
	}

	public selectDay(workingDay, dayTime, event):void{
		console.log('work day');
		console.log(workingDay);
		console.log(dayTime);

		if(!this.selectedDayTimes){
			this.selectedDayTimes = [];
		}

		// reseting this stuff
		let anotherDay = false;
		for(let selectedDayTime of this.selectedDayTimes){
			selectedDayTime.active = false;

			if(selectedDayTime.parentDay.dayIndex != workingDay.dayIndex){
				anotherDay = true;
			}
		}

		if(anotherDay && this.selectedDayTimes.length > 0){

			for(let parentDayTime of this.selectedDayTimes[0].parentDay.times){
				parentDayTime.interimSelected = false;
			}

			for(let selectedDayTime of this.selectedDayTimes){
				selectedDayTime.active = false;
				selectedDayTime.selected = false;
				selectedDayTime.interimSelected = false;
			}

			this.selectedDayTimes = []; 
		}

		for(let workingDayTime of workingDay.times){
			workingDayTime.interimSelected = false;
		}

		dayTime.selected = !dayTime.selected;

		if(dayTime.selected){
			this.selectedDayTimes.push(dayTime);
		}
		else{
			let removeIndex:int = this.selectedDayTimes.indexOf(dayTime, 0);
			if (removeIndex > -1) {
				this.selectedDayTimes.splice(removeIndex, 1);
			}
		}

		this.selectedDayTimes.sort((a,b) => a.dayTimePosition - b.dayTimePosition );

		// activating last selected element
		let selectedDayTimesCount = this.selectedDayTimes.length;
		if( selectedDayTimesCount > 0){
			let lastSelectedDayTimesIndex = selectedDayTimesCount - 1;
			this.selectedDayTimes[lastSelectedDayTimesIndex].active = true;

			if(selectedDayTimesCount > 1){
				// if there are other day times between selected
				let lastDayTimePosition = this.selectedDayTimes[lastSelectedDayTimesIndex].dayTimePosition;
				let firstDayTimePosition = this.selectedDayTimes[0].dayTimePosition;
				let dayTimesInBetween =  lastDayTimePosition - firstDayTimePosition;

				if(dayTimesInBetween > 1 ){
					for(let workingDayTime of workingDay.times){
						if( workingDayTime.dayTimePosition > firstDayTimePosition && workingDayTime.dayTimePosition < lastDayTimePosition  ){
							workingDayTime.interimSelected = true;
						}
					}
				}
			}
		}

	}

	public manageTimeBlockClick(event):void{
		this.showManageTimeBlockModal = true;
		//this.timeBlockTitle = null;
		//this.timeBlockMultiBookingMaxNumber = 1;
		this.timeBlockOptions = [{title: "", multiBookingMaxNumber: 1 }];
		
		if(this.currentBookingTimeBlock){
			 //this.timeBlockTitle =  this.currentBookingTimeBlock.title;
			 //this.timeBlockMultiBookingMaxNumber = this.currentBookingTimeBlock.multiBookingMaxNumber;
			 this.timeBlockOptions = this.currentBookingTimeBlock.options;
		}
	}

	public addTimeBlockOption(event):void{
		if(this.timeBlockOptions){
			this.timeBlockOptions.push( {title: "", multiBookingMaxNumber: 1 } );
		}
	}

	public removeTimeBlockOption(timeBlockOption){
    	var index = this.timeBlockOptions.indexOf(timeBlockOption, 0);
    	if (index > -1) {
      		this.timeBlockOptions.splice(index, 1);
    	}
  	}

	

	public manageTimeBlockOverlayClick(event):void{
		if( !this.modalOverlayClick(event) ) return;

		this.currentBookingTimeBlock = null;
		this.showManageTimeBlockModal = false;

		this.resetSelectedDayTimes();
	}

	public updateTimeBlock():void{
		//this.currentBookingTimeBlock.title =  this.timeBlockTitle; 
		//this.currentBookingTimeBlock.multiBookingMaxNumber = this.timeBlockMultiBookingMaxNumber;
		this.currentBookingTimeBlock.options = this.timeBlockOptions;
		this.timeBlocksChanged();
	}


	public removeTimeBlock():void{
		if(this.currentBookingTimeBlock){
			var index = this.timeBlocks.indexOf(this.currentBookingTimeBlock, 0);
		 	if (index > -1) {
      			this.timeBlocks.splice(index, 1);
    		}
		}
		this.timeBlocksChanged();
	}

	

	public createTimeBlock():void{
		//let newTimeBlock = { title: this.timeBlockTitle, multiBookingMaxNumber: this.timeBlockMultiBookingMaxNumber  };
		let newTimeBlock = {  options : this.timeBlockOptions  };

		let startTime = this.selectedDayTimes[0];
		let selectedDayTimesCount = this.selectedDayTimes.length;
		let endTime = this.selectedDayTimes[ selectedDayTimesCount - 1];

		newTimeBlock.dayIndex = startTime.parentDay.dayIndex;
		newTimeBlock.timeRange = new WdTimeRange(startTime.hour, startTime.minute, endTime.hour, endTime.minute);
		

		this.timeBlocks.push(newTimeBlock);

		
		this.timeBlocksChanged();
	}

	public timeBlocksChanged(){
		this.showManageTimeBlockModal = false;
		this.currentBookingTimeBlock = null;
		this.resetSelectedDayTimes();
		this.renderTimeBlocks();
		this.timeBlocksUpdated.emit( this.timeBlocks );
	}

}	