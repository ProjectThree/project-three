export const LANG_RU_NAME = 'ru';

export const LANG_RU_TRANS = {
    'Monday': 'Понедельник',
    'Tuesday': 'Вторник',
    'Wednesday': 'Среда',
    'Thursday': 'Четверг',
    'Friday': 'Пятница',
    'Saturday': 'Суббота',
    'Sunday': 'Воскресенье',
    'min': 'мин',

    'PENDING': 'ОЖИДАНИЕ', 
	'APPROVED' : 'УТВЕРЖДЕН',  
	'COMPLETE' : 'ВЫПОЛНЕН', 
	'CANCELED' : 'ОТМЕНЕН'
};