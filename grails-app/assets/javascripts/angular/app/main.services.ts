import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ServicesControlModule }  from './services.control.module';

platformBrowserDynamic().bootstrapModule(ServicesControlModule, {});