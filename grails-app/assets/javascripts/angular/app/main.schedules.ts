import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SchedulesModule }  from './schedules.module';

platformBrowserDynamic().bootstrapModule(SchedulesModule, {});