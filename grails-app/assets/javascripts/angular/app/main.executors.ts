import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ExecutorsControlModule }  from './executors.control.module';

platformBrowserDynamic().bootstrapModule(ExecutorsControlModule, {});