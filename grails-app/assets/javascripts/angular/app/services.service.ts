import { Injectable } from '@angular/core';
import { Http, Response, HTTP_PROVIDERS } from '@angular/http';

@Injectable()
export class ServicesService {
	constructor(private http: Http){}

	getServices(){
		return this.http.get(window.wdConfig.routes.findServices);
	}
}