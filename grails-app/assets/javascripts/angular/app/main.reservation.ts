import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ReservationControlModule }  from './reservation.control.module';

platformBrowserDynamic().bootstrapModule(ReservationControlModule, {});