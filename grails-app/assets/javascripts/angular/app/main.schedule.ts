import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ScheduleModule }  from './schedule.module';

platformBrowserDynamic().bootstrapModule(ScheduleModule, {});