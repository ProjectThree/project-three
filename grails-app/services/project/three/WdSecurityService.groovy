package project.three


import grails.transaction.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import com.project.three.*


@Transactional
class WdSecurityService {

    SpringSecurityService springSecurityService

    def getCurrentUser(){
        if(!springSecurityService.isLoggedIn()){
            return null
        }

        def principal = springSecurityService.getAuthentication().getPrincipal()

        if(principal){
            return User.get(principal.id)
        }

    }

    boolean isLoggedIn() {
        return springSecurityService.isLoggedIn()
    }
}
