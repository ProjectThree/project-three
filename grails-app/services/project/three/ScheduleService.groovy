package project.three

import com.project.three.TimeRange
import grails.transaction.Transactional
import java.util.Calendar
import java.text.SimpleDateFormat
import com.project.three.*


@Transactional
class ScheduleService {

    static final daysInWeek = 7

    def generateScheduleItems(Schedule schedule){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        int currentDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        List<TimeRange> timeRanges = schedule.getTimeRanges();
        List<Reservation> reservations = schedule.getNotCanceledReservations()

        def days = [:]
        def daysQuery = []
        for(TimeRange timeRange in timeRanges){
            def times = []

            for (int currentTime = timeRange.fromTime; currentTime <= timeRange.toTime; currentTime+= timeRange.timeStep ) {
                int currentHour = currentTime / 60
                int currentMinute = currentTime % 60
                def timeInfo = [ 'label' : String.format("%02d", currentHour) + ":" +  String.format("%02d", currentMinute), 
                'time' : currentTime, 'hour' : currentHour, 'minute' : currentMinute ]
                times << timeInfo
            }

            def dayInfo = [timeRangeId: timeRange.id, day: timeRange.getDayName(), dayIndex: timeRange.dayOfWeek.getId(),  times: times ];
            daysQuery << dayInfo
            days.put(timeRange.dayOfWeek, dayInfo  );
        }


        // refactor two loops below
        // first loop passing days of this week 
        def finalDays = []
        daysQuery.sort {it.dayIndex }
        for(day in daysQuery){
            if(day.dayIndex >= currentDayOfWeek){
                
                Calendar timeRangeDayCalendar = calendar.clone()
                timeRangeDayCalendar.add(Calendar.DATE, day.dayIndex - currentDayOfWeek)
                day['date'] = timeRangeDayCalendar

                SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
                day['formattedDate'] = dateFormat.format(timeRangeDayCalendar.getTime());

                for(dayTime in day.times){
                    timeRangeDayCalendar.set(Calendar.MINUTE, dayTime.minute)
                    timeRangeDayCalendar.set(Calendar.HOUR_OF_DAY, dayTime.hour)

                    long timeInMills = timeRangeDayCalendar.getTimeInMillis();
                    for(reservation in reservations){
                        if(reservation.isCollide(timeInMills)){
                            dayTime['reserved'] = reservation.id
                            break;
                        }
                    }
                }

                finalDays << day
            }
        }

        // second loop passing days of next week
        for(day in daysQuery){
            if(day.dayIndex < currentDayOfWeek){
                def timeRangeDayCalendar = calendar.clone()
                timeRangeDayCalendar.add(Calendar.DATE, day.dayIndex - currentDayOfWeek + daysInWeek )
                day['date'] = timeRangeDayCalendar;

                SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
                day['formattedDate'] = dateFormat.format(timeRangeDayCalendar.getTime());

                
                for(dayTime in day.times){
                    timeRangeDayCalendar.set(Calendar.MINUTE, dayTime.minute)
                    timeRangeDayCalendar.set(Calendar.HOUR_OF_DAY, dayTime.hour)

                    long timeInMills = timeRangeDayCalendar.getTimeInMillis();
                    for(reservation in reservations){
                        if(reservation.isCollide(timeInMills)){
                            dayTime['reserved'] = reservation.id
                            break;
                        }
                    }
                }

                finalDays << day
            }
        }

        return finalDays;
    }
}
