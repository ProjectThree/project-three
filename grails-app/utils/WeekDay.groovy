package com.project.three

public enum WeekDay {
	SUNDAY(1), 
	MONDAY(2),  
	TUESDAY(3), 
	WEDNESDAY(4), 
	THURSDAY(5), 
	FRIDAY(6), 
	SATURDAY(7) 
 
    final int id
    WeekDay(int id) { this.id = id }

    @Override
    String toString() { id }
    String getKey() { name() }

    public static WeekDay byId(int id) {
        values().find { it.id == id }
    }
}