package com.project.three

public enum OrderStatus {
	PENDING(1), 
	APPROVED(2),  
	COMPLETE(3), 
	CANCELED(4)

    final int id
    OrderStatus(int id) { this.id = id }

    @Override
    String toString() { id }
    String getKey() { name() }

    public static OrderStatus byId(int id) {
        values().find { it.id == id }
    }

    public static OrderStatus byKey(String key) {
        values().find { it.name() == key }
    }
}