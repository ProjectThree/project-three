package com.project.three

import grails.plugin.springsecurity.SpringSecurityService
import java.security.SecureRandom;


class User implements Serializable {

    transient SpringSecurityService springSecurityService

    Date dateCreated // auto-populated
    Date lastUpdated // auto-populated

    String email
    String passwordHash

    String providerName
    String providerId
//    String salt

    Boolean enabled = true

    // user properties
    String firstName
    String lastName

    String phoneNumber

    // company details
    String companyName
    String companyWebsite
    String companyAddress
    String companyPhone

    String password        // plain text, not stored
    String confirm         // plain text, not stored

    static hasMany = [oAuthIDs: OAuthID]

    static constraints = {
        firstName (nullable: true)
        lastName (nullable: true)
        email (nullable: true)
        passwordHash (nullable: true)
        phoneNumber (nullable: true)

        providerName (nullable: true)
        providerId (nullable: true)
        

        companyName (nullable: true)
        companyWebsite (nullable: true)
        companyAddress (nullable: true)
        companyPhone (nullable: true)

        dateCreated (nullable: true)
        lastUpdated (nullable: true)
    }

    static transients = ['password', 'confirm']

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    /*
    List<Place> getPlaces() {
        def places = UserPlace.findAllByUser(this).collect{it.place}
        return places
    }

    //TODO move to service
    List addPlace(Place newPlace, isOwner = false) {
        UserPlace.link(this, newPlace, isOwner)
        return getPlaces()
    }

    //TODO move to service
    List removePlace(Place place) {
        UserPlace.unlink(this, place)
        return getPlaces()
    }
    
    List<Schedule> getSchedules(){
        String query = """
              SELECT s
              FROM com.project.three.Schedule s 
              JOIN s.place as p
              LEFT JOIN com.project.three.UserPlace as up ON up.place.id = p.id
              WHERE up.user = :user 
              
              """

        def schedules = Schedule.executeQuery(query, [user: this])
        return schedules;

    }
    */

    List<Schedule> getSchedules() {
        def schedules = UserSchedule.findAllByUser(this).collect{it.schedule}
        return schedules
    }

    //TODO move to service
    List addSchedule(Schedule newSchedule, isOwner = false) {
        UserSchedule.link(this, newSchedule, isOwner)
        return getSchedules()
    }

    //TODO move to service
    List removeSchedule(Schedule schedule) {
        UserSchedule.unlink(this, schedule)
        return getSchedules()
    }

    Set<Role> getAuthorities() {
        UserRole.findAllByUser(this)*.role
    }

    List<Executor> getExecutors() {
        return Executor.findAllByOwner(this)
    }

    List<Service> getServices() {
        return Service.findAllByOwner(this)
    }

    protected void encodePassword() {
        passwordHash = springSecurityService?.passwordEncoder ?
                springSecurityService.encodePassword(password) :
                password
    }

    public String generatePassword(int len = 16){

      String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_";
      SecureRandom rnd = new SecureRandom();
      StringBuilder sb = new StringBuilder( len );
      for( int i = 0; i < len; i++ ) {
        sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
      }
      String newPassword = sb.toString();
      this.password = newPassword

      return newPassword
    }

    static User getByProviderDetails(String providerName, String providerId) {
        return User.findByProviderNameAndProviderId(providerName, providerId)

    }
}
