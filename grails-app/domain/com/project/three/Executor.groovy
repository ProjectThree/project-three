package com.project.three

class Executor {

    String name
    Date dateCreated
    User owner

//    List<TimeRange> timeRanges

    static constraints = {
    	name (nullable: true)
        dateCreated (nullable: true)
    }

}
