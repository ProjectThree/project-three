package com.project.three

class Service {

    User owner

    String name
    Date dateCreated
    int duration // in min
    float price

    static constraints = {
        name (nullable: true)
        dateCreated (nullable: true)
        duration (nullable: true)
        price (nullable: true)
    }

    
}
