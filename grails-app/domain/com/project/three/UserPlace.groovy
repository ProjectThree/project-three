package com.project.three

class UserPlace {

    User user
    Place place

    Boolean isOwner

    static mapping = {
        user lazy: false
    }

    static constraints = {
        isOwner (nullable: true)
    }


    static UserPlace link(newUser, newPlace, isOwner = false) {
        def userPlace = UserPlace.findByUserAndPlace(newUser, newPlace)
        if (!userPlace){
            userPlace = new UserPlace()
            userPlace.isOwner = isOwner
            userPlace.place = newPlace
            userPlace.user = newUser
            userPlace.save()
        }
        return userPlace
    }

    static void unlink(oldUser, oldPlace) {
        def userPlace = UserPlace.findByUserAndPlace(oldUser, oldPlace)
        if (userPlace){
            userPlace.delete()
        }
    }

}
