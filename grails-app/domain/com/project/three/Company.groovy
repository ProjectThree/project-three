package com.project.three

class Company {

    User owner

    static constraints = {
    }

    List<Place> getPlaces() {
        return Place.findAllByCompany(this)
    }
}
