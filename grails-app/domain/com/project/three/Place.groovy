package com.project.three

class Place {

    String title
    String address
    String phoneNumber

    Date dateCreated

    static constraints = {
        title (nullable: true)
        address (nullable: true)
        phoneNumber (nullable: true)

        dateCreated (nullable: true)
    }

    List getManagers() {
        def managers = UserPlace.findAllByPlace(this).collect{it.user}
        return managers
    }

    Boolean hasManager(user) {
        def managers = UserPlace.findAllByPlace(this).collect{it.user}

        return managers.contains(user)
    }

    List addManager(Place newPlace, isOwner = false) {
        UserPlace.link(this, newPlace, isOwner)
        return getManagers()
    }

    List removeManager(Place place) {
        UserPlace.unlink(this, place)
        return getManagers()
    }


    List<Executor> getExecutors() {
        return Executor.findAllByLocation(this)
    }

    List<Schedule> getSchedules() {
        return Schedule.findAllByPlace(this)
    }

}
