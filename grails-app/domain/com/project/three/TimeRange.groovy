package com.project.three

class TimeRange {

    Schedule schedule

    WeekDay dayOfWeek
    //TODO find Time object
    int fromTime
    int toTime

    int timeStep

    static constraints = {
    	timeStep (nullable: true)
    	fromTime (nullable: true)
    	toTime (nullable: true)
    }

    String getDayName(){
         return ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][ dayOfWeek.id - 1 ];
    }


    List<Place> getServices() {
        def services = TimeRangeService.findAllByTimeRange(this).collect{it.service}
        return services
    }

}
