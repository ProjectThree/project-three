package com.project.three

class UserSchedule {

    User user
    Schedule schedule

    Boolean isOwner

    static mapping = {
        user lazy: false
    }

    static constraints = {
        isOwner (nullable: true)
    }


    static UserSchedule link(newUser, newSchedule, isOwner = false) {
        def userSchedule = UserSchedule.findByUserAndSchedule(newUser, newSchedule)
        if (!userSchedule){
            userSchedule = new UserSchedule()
            userSchedule.isOwner = isOwner
            userSchedule.schedule = newSchedule
            userSchedule.user = newUser
            userSchedule.save()
        }
        return userSchedule
    }

    static void unlink(oldUser, oldSchedule) {
        def userSchedule = UserSchedule.findByUserAndSchedule(oldUser, oldSchedule)
        if (userSchedule){
            userSchedule.delete()
        }
    }

}
