package com.project.three
import java.security.SecureRandom;

class Schedule {

    String title
    String code
    String config
    Date dateCreated
    int version
    Boolean confidential

    static mapping = {
      config type: 'text'
    }

    static constraints = {
        title (nullable: true)
        code (nullable: true)
        dateCreated (nullable: true)
        version (nullable: true)
        confidential (nullable: true)
    }

    //List<TimeRange> getTimeRanges() {
    //    return TimeRange.findAllBySchedule(this)
    //}

    List<Reservation> getReservations() {
        return Reservation.findAllBySchedule(this)
    }

    String generateCode(){
      Boolean isUnique = false;
      String newCode = "";
      while(!isUnique)  {
        newCode = this.randomString(8);
        def foundSchedule = Schedule.findByCode(newCode);
        if(!foundSchedule){
          isUnique = true;
          break;
        }
      }

      this.setCode(newCode);

      return newCode;

    }


    

    String randomString( int len ){
      String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
      SecureRandom rnd = new SecureRandom();
      StringBuilder sb = new StringBuilder( len );
      for( int i = 0; i < len; i++ ) {
        sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
      }
      return sb.toString();
    }

    List<Reservation> getNotCanceledReservations() {
         String query = """
              SELECT r
              FROM com.project.three.Reservation r 
              WHERE r.schedule = :schedule
              AND r.status != :canceledStatus
              """
              
        return Reservation.executeQuery(query, [schedule: this, canceledStatus: OrderStatus.CANCELED])
    }


    List getManagers() {
        def managers = UserSchedule.findAllBySchedule(this).collect{it.user}
        return managers
    }

    Boolean hasManager(user) {
        def managers = UserSchedule.findAllBySchedule(this).collect{it.user}

        return managers.contains(user)
    }

    List addManager(User newUser, isOwner = false) {
        UserSchedule.link(newUser, this, isOwner)
        return getManagers()
    }

    List removeManager(User user) {
        UserSchedule.unlink(user, this)
        return getManagers()
    }

    
}
