package com.project.three
import org.springframework.context.i18n.LocaleContextHolder as LCH

class Reservation {

    def messageSource 

    Date dateCreated
    Date dateReserved
    int sessionDuration // in minutes
    String serviceName
    String config

    String clientEmail
    String clientPhone
    String clientName

    Executor executor
    Schedule schedule
    Service service

    OrderStatus status
    float price

    Boolean multiBooking
    int timeBlockOptionIndex;

    static mapping = {
      config type: 'text'
    }


    static constraints = {
        service (blank: true, nullable: true)
        executor (blank: true, nullable: true)
        status (nullable: true)
        schedule (nullable: true)
        dateCreated (nullable: true)
        sessionDuration (nullable: true)
        serviceName (nullable: true)
        price (nullable: true)
        config (nullable: true)
        multiBooking( nullable: true)
        timeBlockOptionIndex(nullable: true)
        clientEmail(nullable: true)
        clientPhone(nullable: true)
        clientName(nullable: true)
    }    

    long getStartTimeInMills(){
        return dateReserved.getTime()
    }

    long getEndTimeInMills(){
        return dateReserved.getTime() + sessionDuration * 60 * 1000
    }

    boolean isCollide(mills){
        return getStartTimeInMills() <= mills && mills <= getEndTimeInMills()
    }

    String getStatusLabel(){
        String statusKey = this.status.getKey()
        messageSource.getMessage(statusKey, null, null, LCH.getLocale())
    }
}
