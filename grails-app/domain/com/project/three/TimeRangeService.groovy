package com.project.three

class TimeRangeService {

    TimeRange timeRange
    Service service

    
    static mapping = {
        user lazy: false
    }

    static constraints = {
    
    }


    static TimeRangeService link(inputTimeRange, inputService) {
        def timeRangeService = TimeRangeService.findByTimeRangeAndService(inputTimeRange, inputService)
        if (!timeRangeService){
            timeRangeService = new TimeRangeService()
            timeRangeService.timeRange = inputTimeRange
            timeRangeService.service = inputService
            timeRangeService.save()
        }
        return timeRangeService
    }

    static void unlink(inputTimeRange, inputService) {
        def timeRangeService = TimeRangeService.findByUserAndPlace(inputTimeRange, inputService)
        if (timeRangeService){
            timeRangeService.delete()
        }
    }

}
