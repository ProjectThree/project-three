package project.three

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/desk/$code" {
            controller = "reservation"
            action = "scheduleByCode"
        }

        "/places" {
            controller = "place"
            action = "index"
        }

        "/" {
            controller = "main"
            action = "index"
        }

        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
