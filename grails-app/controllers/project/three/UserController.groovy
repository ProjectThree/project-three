package project.three

import com.project.three.*


import com.github.scribejava.core.model.OAuth2AccessToken
import com.sun.istack.internal.Nullable
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.SpringSecurityUtils
import org.springframework.security.core.authority.AuthorityUtils
import grails.plugin.springsecurity.annotation.Secured
import grails.plugin.springsecurity.oauth2.exception.OAuth2Exception
import grails.plugin.springsecurity.oauth2.token.OAuth2SpringToken

import org.springframework.security.core.Authentication
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import grails.plugin.springsecurity.oauth2.SpringSecurityOauth2BaseService 

import grails.plugin.springsecurity.userdetails.GrailsUser
import org.apache.commons.lang.StringUtils
import org.apache.commons.lang.exception.ExceptionUtils
import org.grails.validation.routines.UrlValidator
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.servlet.ModelAndView

import com.project.three.Role
import com.project.three.User

@Secured('permitAll')
class UserController {

	WdSecurityService wdSecurityService
	SpringSecurityService springSecurityService
	SpringSecurityOauth2BaseService springSecurityOauth2BaseService
	public static final String SPRING_SECURITY_OAUTH_TOKEN = 'springSecurityOAuthToken'

	def logout = {
		session.invalidate()
		redirect(controller:'user', action: "signin")
	}

	def index() {
		//return redirect(controller: "user", action: "signin")
		redirect(controller:'places')
	}

	def changeLocale(){

		session['org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE'] = new Locale(params.lang)
		if (request.xhr) {
			return render(text: params.lang)
		}
		redirect(controller:'places')
	}

	def signup () {
		
		if( request.method == "POST"){
			
			User user = new User(params)
			/*
			user.password = params.password
			user.email = params.email
			user.firstName = params.firstName
			user.lastName = params.lastName
			*/
			user.dateCreated = new Date()

			if(user.password != params.confirm){
				user.errors.rejectValue("password", "Confirm password must match")
				return [user:user]
			}
			else if(user.save()){
				def userRole = Role.findByAuthority(Role.USER) ?: new Role(authority: Role.USER).save(failOnError: true)
				UserRole.create user, userRole
				redirect(controller: "places")
			}
			else{
				return [user: user]
			}
		}
		
	}

	def signin () {
		//TODO show error from srping LoginController
		if( wdSecurityService.isLoggedIn() ){
			redirect(controller: "places")
			return	
		}
		
	}

	def ask() {
		// check with https://github.com/MatrixCrawler/grails-spring-security-oauth2/blob/master/grails-app/controllers/grails/plugin/springsecurity/oauth2/SpringSecurityOAuth2Controller.groovy
        if (springSecurityService.isLoggedIn()) {
            //def currentUser = springSecurityService.currentUser
            //OAuth2SpringToken oAuth2SpringToken = session[SPRING_SECURITY_OAUTH_TOKEN] as OAuth2SpringToken
            // Check for token in session
            /*
            if (!oAuth2SpringToken) {
                log.warn("ask: OAuthToken not found in session")
                throw new OAuth2Exception('Authentication error')
            }
            // Try to add the token to the OAuthID's
            currentUser.addTooAuthIDs(
                    provider: oAuth2SpringToken.providerName,
                    accessToken: oAuth2SpringToken.socialId,
                    user: currentUser
            )
            if (currentUser.validate() && currentUser.save()) {
                // Could assign the token to the OAuthIDs. Login and redirect
                oAuth2SpringToken = springSecurityOauth2BaseService.updateOAuthToken(oAuth2SpringToken, currentUser)
                authenticateAndRedirect(oAuth2SpringToken, getDefaultTargetUrl())
                return
            }
            */
        }
        OAuth2SpringToken oAuth2SpringToken = session[SPRING_SECURITY_OAUTH_TOKEN] as OAuth2SpringToken
        String providerName = oAuth2SpringToken.providerName
        String providerId = oAuth2SpringToken.socialId

        User user = User.getByProviderDetails(providerName, providerId)
        if(!user){
        	user = new User()	
        	user.providerName = providerName
        	user.providerId = providerId
        	user.generatePassword()
        	user.save()

        }
 
        Authentication authentication = new UsernamePasswordAuthenticationToken(
          user, null,
            AuthorityUtils.createAuthorityList("ROLE_USER"));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		

		redirect(controller: "places")
		

        return render("text": "error")
    }

    
	
}
