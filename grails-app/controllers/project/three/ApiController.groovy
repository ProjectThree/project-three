package project.three

import com.project.three.*
import grails.plugin.springsecurity.annotation.Secured

import java.util.Calendar
import java.text.SimpleDateFormat

@Secured('permitAll')
class ApiController {
	
	def routes () {
		render text: """
                    window.wdConfig = {
                    	routes: {
                    		findServices: "${createLink(controller: 'executor', action: 'findServices')}",
                    		currentDate: "${createLink(controller: 'api', action: 'currentDate')}",
                    		reservationAddJson: "${createLink(controller: 'reservation', action: 'addJson')}",
                    		scheduleSaveJson: "${createLink(controller: 'place', action: 'scheduleSaveJson')}",
                    		scheduleJson: "${createLink(controller: 'reservation', action: 'schedule')}",
                            scheduleReservations: "${createLink(controller: 'reservation', action: 'scheduleReservations')}",

                            schedules: "${createLink(controller: 'place', action: 'schedules')}",
                            
                            executors: "${createLink(controller: 'place', action: 'executors')}",
                            executorSaveJson: "${createLink(controller: 'place', action: 'executorSaveJson')}",

                            services: "${createLink(controller: 'place', action: 'services')}",
                            servicesSaveJson: "${createLink(controller: 'place', action: 'servicesSaveJson')}",
                            

                            reservations: "${createLink(controller: 'place', action: 'reservations')}",
                            updateReservationStatus : "${createLink(controller: 'place', action: 'updateReservationStatus')}",
                    	}
                    };
                """,
       contentType: 'js'
	}

	def currentDate(){
		Calendar calendar = Calendar.getInstance();
		//calendar.set(Calendar.DAY_OF_WEEK, 1); // testing
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        String  formatedDate = dateFormat.format(calendar.getTime());
		render text: formatedDate,
		contentType: 'text/plain'
	}

}
