package project.three

import com.project.three.*
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

import java.util.Calendar
import java.text.SimpleDateFormat

import grails.converters.JSON

@Secured([Role.USER])
class PlaceController {

	SpringSecurityService springSecurityService
	WdSecurityService wdSecurityService

	def list(){
		def user = wdSecurityService.getCurrentUser()
		return [user: user, places: user?.getPlaces() ]
	}

	def index(){
		def user = wdSecurityService.getCurrentUser()
		return [user: user ]
	}

	def scheduleWizard(){
		def user = session.user

		return [user: user, scheduleId : params.id ]
	}

	def reservations(){

		def user = wdSecurityService.getCurrentUser()

		String query = """
			  SELECT r
			  FROM com.project.three.Reservation r 
			  LEFT JOIN com.project.three.UserSchedule as us ON us.schedule.id = r.schedule.id
			  WHERE us.user = :user 
			  
			  """

		def reservations = Reservation.executeQuery(query, [user: user])


		if(params.output == 'json'){
			def output = []
			for(reservation in reservations){
				SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
				def formatedReservationDate = dateFormat.format(reservation.dateReserved.getTime());

				

				def reservationItem = [
					'id': reservation.id,
					'sessionDuration': reservation.sessionDuration,
					'status': reservation.status.getKey(),
					'dateReserved': formatedReservationDate,
					'clientName': reservation.clientName,
					'clientEmail': reservation.clientEmail,
					'clientPhone': reservation.clientPhone,
				];

				if(reservation.service && reservation.service.title){
					reservationItem['service'] = [ 'title': reservation.service.title ]
				} 
				else if(reservation.serviceName){
					reservationItem['serviceName'] = reservation.serviceName
				}

				if(reservation.price){
					reservationItem['price'] = reservation.price;
				}

				if(reservation.multiBooking){
					reservationItem['multiBooking'] = reservation.multiBooking;
				}


				/*if(reservation.location){
					reservationItem['location'] = 
						[
					 		'title' : reservation.location.title,
					 		'id' : reservation.location.id
						];
				}*/

				output << reservationItem
			}

			render (output as JSON)
			return
		}

		return [user: user, reservations: reservations ]
	}

	def updateReservationStatus(){

		def user = wdSecurityService.getCurrentUser()
		def inputData = request.JSON

		if( request.method == "POST"){
			def reservation = Reservation.findById(inputData.id)
			if(reservation){
				if(reservation.schedule && reservation.schedule.place && !reservation.schedule.place.hasManager(user)){
        			return render(status: 503, text: 'Permission error')	
        		}
				reservation.status = OrderStatus.byKey(inputData.status)
				reservation.save()
			}
			
			return render(text: 'updated')
		}


		return render(status: 503, text: 'Server error')
	}

	/*
	def updateReservationStatus(){
		def reservation = Reservation.findById(params.id)
		if(reservation){
			reservation.status = OrderStatus.byId(params.status.toInteger())
			reservation.save()
		}
		return redirect(controller: "place", action: "reservations")
	}
	*/

	def edit () {
		def user = wdSecurityService.getCurrentUser()
		def place = Place.findById(params.id)
		if(!place){
			return redirect(controller: "place", action: "index")
		}

		if(place && !place.hasManager(user)){
        	return render(status: 503, text: 'Permission error')	
        }

		if( request.method == "POST"){
			place.properties = params
			place.save()

			flash.message = "Place has been updated"
		}

		return [place:place]
	}

	def add () {
		def user = wdSecurityService.getCurrentUser()
		if( request.method == "POST"){
			def place = new Place(params);
			//TODO fix me
//			place.owner = user
			place.dateCreated = new Date()
			if(place.save()){
				//user.addPlace(place)
				UserPlace.link(user, place, true)
				//user.addPlace(place, true)
				return redirect(controller: "place", action: "index")
			}
			else{
				return [place: place]
			}
		}
	}

	def executors(){
        def user = wdSecurityService.getCurrentUser()
        def executors = user.getExecutors();

        if(params.output == 'json'){
			def output = []
			for(executor in executors){
				
				def executorItem = [
					'id': executor.id,
					'name': executor.name,
				];

				output << executorItem
			}

			render (output as JSON)
			return
		}

        return [user: user, executors: executors]
    }

    def services(){
        def user = wdSecurityService.getCurrentUser()
        def services = user.getServices();

        if(params.output == 'json'){
			def output = []
			for(service in services){
				
				def serviceItem = [
					'id': service.id,
					'name': service.name,
					'duration': service.duration,
					'price': service.price
				];

				output << serviceItem
			}

			render (output as JSON)
			return
		}

        return [user: user, services: services]
    }

	def schedules(){
        def user = wdSecurityService.getCurrentUser()
        def result = [:]
        def schedules
        /*if(params.placeId){
            def place = Place.findById(params.placeId)
            if(place){
            	if(!place.hasManager(user)){
            		 return render(status: 503, text: 'Permission error')	
            	}
            	schedules = place.getSchedules()
            	result = [place: place, schedules: schedules];
            }
        }
        else 
        */
        //println user.id
        if(user){
        	schedules = user.getSchedules()
        	result = [user: user, schedules: schedules];
        }


        if(params.output == 'json'){
			def output = []
			for(schedule in schedules){
				
				def scheduleItem = [
					'id': schedule.id,
					'title': schedule.title,
					'code': schedule.code,
					'confidential': schedule.confidential
				];

				output << scheduleItem
			}

			render (output as JSON)
			return
		}

		return result
    }

    def removeSchedule(){
    	def user = wdSecurityService.getCurrentUser()
    	def scheduleId = params.id
    	if(scheduleId){
    		def schedule = Schedule.findById(scheduleId)	
    		if(schedule){
    			def place = schedule.place
    			if(!(place && place.hasManager(user) ) ) {
            		 return render(status: 503, text: 'Permission error')	
            	}
    			schedule.delete()
    			return redirect(controller: "place", action: "schedules", params: [placeId: place.id ] )
    		}
    	}

    	return render(status: 503, text: 'Server error')
    }

    def removeExecutor(){
    	def user = wdSecurityService.getCurrentUser()
    	def executorId = params.id
    	if(executorId){
    		def executor = Executor.findById(executorId)	
    		if(executor){
    			if( executor.owner != user  ) {
            		 return render(status: 503, text: 'Permission error')	
            	}
    			executor.delete()
    			return redirect(controller: "place", action: "executors" )
    		}
    	}

    	return render(status: 503, text: 'Server error')
    }

    def removeService(){
    	def user = wdSecurityService.getCurrentUser()
    	def serviceId = params.id
    	if(serviceId){
    		def service = Service.findById(serviceId)	
    		if(service){
    			if( service.owner != user  ) {
            		 return render(status: 503, text: 'Permission error')	
            	}
    			service.delete()
    			return redirect(controller: "place", action: "services" )
    		}
    	}

    	return render(status: 503, text: 'Server error')
    }

    def executorSaveJson () {
		def user = wdSecurityService.getCurrentUser()
		def inputData = request.JSON

		def output = [:]

		if( request.method == "POST"){
			def executor = Executor.findById(inputData.executor_id)
            if(executor && executor.owner != user ){
            	return render(status: 503, text: 'Permission error')	
            }

            def isNew = false;
            if(!executor){
            	executor = new Executor();
            	executor.dateCreated = new Date()
            	executor.owner = user
            	isNew = true;
            }

            executor.name = inputData.name

            if(executor.save(flush: true, failOnError: true) ){
                output['executor_id'] = executor.id
            }
            else{
                output['error'] = 'error while saving';
            }
            	
		}

		render (output as JSON)

	}


	def servicesSaveJson () {
		def user = wdSecurityService.getCurrentUser()
		def inputData = request.JSON

		def output = [:]

		if( request.method == "POST"){
			def service = Service.findById(inputData.service_id)
            if(service && service.owner != user ){
            	return render(status: 503, text: 'Permission error')	
            }

            def isNew = false;
            if(!service){
            	service = new Service();
            	service.dateCreated = new Date()
            	service.owner = user
            	isNew = true;
            }

            service.name = inputData.name
            
            if(inputData.price){
            	service.price = inputData.price
            }

            if(inputData.duration){
            	service.duration = inputData.duration
        	}

            if(service.save(flush: true, failOnError: true) ){
                output['service_id'] = service.id
            }
            else{
                output['error'] = 'error while saving';
            }
            	
		}

		render (output as JSON)

	}

    

	def scheduleSaveJson () {
		def user = wdSecurityService.getCurrentUser()
		def inputData = request.JSON
		def output = [:]


		if( request.method == "POST"){
			/*
			def place = Place.findById(inputData.place_id)
			if(place && !place.hasManager(user)){
            	return render(status: 503, text: 'Permission error')	
			}
			if(!place && inputData.forSaving.newPlaceTitle){
				place = new Place();
				place.dateCreated = new Date()				
				place.title = inputData.forSaving.newPlaceTitle
				place.address = inputData.forSaving.newPlaceAddress
				place.phoneNumber = inputData.forSaving.newPlacePhoneNumber 
				place.save()
				UserPlace.link(user, place, true)
			}

			if(!place){
				render ([error: 'no place found' ] as JSON)
			}
			*/
			
            def schedule = Schedule.findById(inputData.schedule_id)
            if(schedule && !schedule.hasManager(user)){
            	return render(status: 503, text: 'Permission error')	
            }

            def isNew = false;
            if(!schedule){
            	schedule = new Schedule();
            	schedule.dateCreated = new Date()
            	//schedule.place = place
            	schedule.generateCode()
            	isNew = true;
            }
    		
    		//schedule.code
    		inputData.forSaving = null;
    		schedule.config = inputData as JSON 
    		schedule.title  = inputData.title
    		if(inputData.confidential){
    			schedule.confidential = true
    		}
    		
    		schedule.version = 1

            if(schedule.save(flush: true, failOnError: true) ){
            	if(isNew){
            		schedule.addManager(user, true)
            	}
                output['schedule_id'] = schedule.id
            }
            else{
                output['error'] = 'error while saving';
            }
            
        }


        render (output as JSON)
	}
}
