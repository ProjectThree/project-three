package project.three

import com.project.three.*
import grails.plugin.springsecurity.annotation.Secured

@Secured('permitAll')
class TemplatesController {

	def scheduleWizard() {
		
	}

	def scheduleRender() {
		
	}

	def reservationControl() {
		
	}

	def schedulesControl() {
		
	}

	def executorsControl() {
		
	}

	def servicesControl() {
		
	}

	

	
}
