package project.three

import com.project.three.*
import grails.plugin.springsecurity.annotation.Secured
import grails.plugin.springsecurity.SpringSecurityService

import com.project.three.Role
import com.project.three.User

@Secured('permitAll')
class MainController {
	SpringSecurityService springSecurityService
	WdSecurityService wdSecurityService

	def index() {
		def user = wdSecurityService.getCurrentUser()
		if(user){
			redirect(controller:'place', action: 'index')	
		}
	}
	
}
