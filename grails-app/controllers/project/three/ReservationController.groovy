package project.three

import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON
import java.util.Calendar
import java.text.SimpleDateFormat

import com.project.three.*


@Secured('permitAll')
class ReservationController {

	//ScheduleService scheduleService
	
    def schedule () {
        def schedule = null
        if(params.id){
            schedule = Schedule.findById(params.id)
        }
        else if(params.code){
            schedule = Schedule.findByCode(params.code)
        }

        if(!schedule){
            return render(status: 503, text: 'No schedule found')   
        }

        if(params.config){
            render text: schedule.config, contentType: 'js'
            return
        }
        
        //def scheduleItems = scheduleService.generateScheduleItems(schedule)//timeRanges)
        return [schedule: schedule]
    }

    def scheduleByCode(){
        def schedule = null
        if(params.code){
            schedule = Schedule.findByCode(params.code)
        }

        if(!schedule){
            return render(status: 503, text: 'No schedule found')   
        }

        if(params.config){
            render text: schedule.config, contentType: 'js'
            return
        }
        
        render(view: "schedule", model: [schedule: schedule] )
        
    }

    def show () {
        def reservation = Reservation.findById(params.reservationId)
        return [reservation: reservation]
    }    

    def scheduleReservations() {

        if(params.schedule_id){
            def schedule = Schedule.findById(params.schedule_id)
            if(schedule){
                String query = """
                SELECT r
                FROM com.project.three.Reservation r 
                WHERE r.schedule = :schedule AND r.status != :canceledStatus AND r.dateReserved > :currentDate

                """
                def currentDate = new Date();
                def reservations = Reservation.executeQuery(query, [currentDate :currentDate,  schedule: schedule, canceledStatus: OrderStatus.CANCELED ])

                if(params.output == 'json'){
                    def output = []
                    for(reservation in reservations){
                        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
                        def formatedReservationDate = dateFormat.format(reservation.dateReserved.getTime());



                        def reservationItem = [
                        'id': reservation.id,
                        'sessionDuration': reservation.sessionDuration,
                        'status': reservation.status.getKey(),
                        'dateReserved': formatedReservationDate,
                        'timeBlockOptionIndex': (reservation.timeBlockOptionIndex ? reservation.timeBlockOptionIndex : 0)
                        ];

                        if(reservation.service && reservation.service.title){
                            reservationItem['service'] = [ 'title': reservation.service.title ]
                        } 
                        else if(reservation.serviceName){
                            reservationItem['serviceName'] = reservation.serviceName
                        }


                        output << reservationItem
                    }

                    render (output as JSON)
                    return
                }
                
            }
        }
        

        return render(status: 503, text: 'No schedule found')   
    }  

    


    def addJson() {
        def data = request.JSON
        def newReservationId = null;
        if (request.method == "POST"){
            def schedule = Schedule.findById(data.schedule_id)
            if(!schedule){
                return render(status: 503, text: 'No schedule found')   
            }

            def date = Date.parse("yyyy-MM-dd H:m", data.time)
            def reservation = new Reservation();
            reservation.dateCreated = new Date()
            reservation.status = OrderStatus.PENDING
            
            reservation.schedule = schedule
            //reservation.location = schedule.place

            def config = [:]
            if(data.timeBlockTitle){
                config['timeBlockTitle'] = data.timeBlockTitle;
            }

            config['timeBlockOptionIndex'] = data.timeBlockOptionIndex;
            reservation.timeBlockOptionIndex = data.timeBlockOptionIndex;

            reservation.config = config as JSON;

            if(data.clientInfo){
                reservation.clientName = data.clientInfo.clientName ?: null;
                reservation.clientEmail = data.clientInfo.clientEmail ?: null;
                reservation.clientPhone = data.clientInfo.clientPhone ?: null;
            }

            reservation.serviceName = data.serviceName
            
            if(data.duration > 0){
                reservation.sessionDuration = data.duration
            }

            if(data.price > 0){
                reservation.price = data.price
            }

            if(data.maxBookingNumber > 1){
                reservation.multiBooking = true;
            }

            reservation.dateReserved = date
            if(reservation.save(flush: true, failOnError: true)){
                newReservationId = reservation.id
                def output = [reservation_id : reservation.id ]
                def outputJs = output as JSON
                render text:  outputJs, contentType: 'js'
                return
            }
            
        }

        return render(status: 503, text: 'Server error')   
    }

    def add() {
        def schedule = Schedule.findById(params.scheduleId);
        def executor = schedule.executor
        //def place = executor.location
        def time = params.time

        def timeRange = TimeRange.findById(params.timeRangeId)

        //LocalTime inputTime = LocalTime.parse(time)
        def date = Date.parse("yyyy-MM-dd H:m", time)
        

        // remake
        //if(place.owner.id != user.id){
        //    return render(status: 503, text: 'No permission')
        //}

        if (request.method == "POST"){
       	 	def reservation = new Reservation();

            //reservation.location = place
            reservation.executor = executor
            reservation.schedule = schedule
            reservation.dateCreated = new Date()
            reservation.status = OrderStatus.PENDING
            def service = Service.findById(params.serviceId);
            reservation.sessionDuration = service.duration // min, for test 
            reservation.dateReserved = date

            

            reservation.service = service

            //return render(text: "done");

            if(reservation.save()){
                return redirect(controller: "reservation", action: "show", params: [reservationId: reservation.id ] )
            }
            else{
                return [schedule: schedule, time: time, timeRange: timeRange ]
            }	
        }

        return [schedule: schedule, time: time, timeRange: timeRange ]
    } 
	
}
