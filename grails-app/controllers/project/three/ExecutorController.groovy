package project.three

import com.project.three.*
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON

import java.time.LocalTime

@Secured([Role.USER])
class ExecutorController {

    WdSecurityService wdSecurityService
    SpringSecurityService springSecurityService
    ScheduleService scheduleService

    def executors(){

        def user = wdSecurityService.getCurrentUser()
        if(params.placeId){
            def place = Place.findById(params.placeId)
            // remake
            //if(place.owner.id != user.id){
            //    return render(status: 503, text: 'No permission')
            //}    

            def executors = place.getExecutors()

            return [place: place, executors: executors];
        }
    }

    def edit () {
        def executor = Executor.findById(params.id)
        if(!executor){
            return redirect(controller: "place", action: "index")
        }

        def user = wdSecurityService.getCurrentUser()
        def userPlaces = user.getPlaces()

        def isExecutorBelongsToUser = true; // REMAKE

        /*
        userPlaces.each { userPlace ->
            if(userPlace.id == executor.location.id){
                isExecutorBelongsToUser = true
                return
            }
        }*/

        if(!isExecutorBelongsToUser){
            return render(status: 503, text: 'No permission')
        }


        if( request.method == "POST"){
            executor.properties = params
            executor.save()

            flash.message = "Executor has been updated"
        }

        return [executor: executor]
    }


    def add () {
        def place = Place.findById(params.placeId)
        def user = wdSecurityService.getCurrentUser()

        // remake
        //if(place.owner.id != user.id){
        //    return render(status: 503, text: 'No permission')
        //}

        if( request.method == "POST"){
            def executor = new Executor(params);

            executor.location = place
            executor.dateCreated = new Date()

            if(executor.save()){
                return redirect(controller: "executor", action: "list", params: [placeId: place.id ] )
            }
            else{
                return [executor: executor, place: place]
            }
        }

        return [place: place]


    }

    def schedule () {
        def schedule = Schedule.findById(params.scheduleId)
        //List<TimeRange> timeRanges = schedule.getTimeRanges();
        def scheduleItems = scheduleService.generateScheduleItems(schedule)
        return [schedule: schedule, scheduleItems: scheduleItems ]
    }

    def addTimeRange () {
        def schedule = Schedule.findById(params.scheduleId);
        def executor = schedule.executor
        def user = wdSecurityService.getCurrentUser()


        
        // remake
        //if(place.owner.id != user.id){
        //    return render(status: 503, text: 'No permission')
        //}

        if (request.method == "POST"){
            LocalTime inputTimeFrom = LocalTime.parse(params.from_time)
            LocalTime inputTimeTo = LocalTime.parse(params.to_time)

            

            def timeRange = new TimeRange(
                    dayOfWeek: params.dayOfWeek,
                    timeStep: params.timeStep.toInteger(),
                    schedule: schedule,
                    fromTime: inputTimeFrom.toSecondOfDay() / 60,
                    toTime: inputTimeTo.toSecondOfDay() / 60
            );

            if (timeRange.save()) {

                for(serviceId in params.services){
                    
                    def service = Service.findById(serviceId)
                    if(service){
                        TimeRangeService.link(timeRange,  service)
                    }    
                }

                log.info "time range saved"
                return redirect(action: "schedules", params: [executorId: executor.id ] )
            } else {
                log.error "time range not saved"
                timeRange.errors.each {
                    println it
                }
                //TODO show errors on page
                return [schedule: schedule, timeRange: timeRange, executor: executor]
            }
        }

        return [schedule: schedule, executor:executor]
    }


    def schedules(){
        def user = wdSecurityService.getCurrentUser()
        if(params.executorId){
            def executor = Executor.findById(params.executorId)
            // remake
            //if(place.owner.id != user.id){
            //    return render(status: 503, text: 'No permission')
            //}    

            def schedules = executor.getSchedules()

            return [place: executor.location, executor: executor, schedules: schedules];
        }
    }

    def addSchedule () {
        def executor = Executor.findById(params.executorId)
        def user = wdSecurityService.getCurrentUser()

        // remake
        //if(place.owner.id != user.id){
        //    return render(status: 503, text: 'No permission')
        //}

        if( request.method == "POST"){
            def schedule = new Schedule(params);

            schedule.executor = executor
            schedule.dateCreated = new Date()

            if(schedule.save()){
                return redirect(controller: "executor", action: "schedules", params: [executorId: executor.id ] )
            }
            else{
                return [executor: executor, schedule: schedule]
            }
        }

        return [executor: executor]
    }

    def services(){
        def user = wdSecurityService.getCurrentUser()
        if(params.executorId){
            def executor = Executor.findById(params.executorId)
            // remake
            //if(place.owner.id != user.id){
            //    return render(status: 503, text: 'No permission')
            //}    

            def services = executor.getServices()

            return [place: executor.location, executor: executor, services: services];
        }
    }

    def findServices(){
        def services = Service.getAll()
        def responseData = [];
        for(Service service in services){
            responseData << [ name : service.title ];
        }
        render responseData as JSON   
    }

    def addService () {
        def executor = Executor.findById(params.executorId)
        def user = wdSecurityService.getCurrentUser()

        // remake
        //if(place.owner.id != user.id){
        //    return render(status: 503, text: 'No permission')
        //}

        if( request.method == "POST"){
            def service = new Service(params);

            service.executor = executor
            service.dateCreated = new Date()

            if(service.save()){
                return redirect(controller: "executor", action: "services", params: [executorId: executor.id ] )
            }
            else{
                return [executor: executor, service: service]
            }
        }

        return [executor: executor]
    }
}
